#ifndef EIGENLIB_H
#define EIGENLIB_H

#include <Eigen/Sparse>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <Eigen/SparseCholesky>
#include <Eigen/SparseLU>
#include <Eigen/SparseQR>
#include <Eigen/OrderingMethods>
#include <Eigen/LU>
#include <Eigen/IterativeLinearSolvers>

typedef Eigen::SparseMatrix<double,Eigen::RowMajor> SpMat;
typedef Eigen::Triplet<double> T;
struct Triplets{
    int row;
    int col;
    double val;
};

struct Duplets{
    int index;
    double weight;
};

#endif /* EIGENLIB_H */
