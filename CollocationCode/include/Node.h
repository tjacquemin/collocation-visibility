#ifndef NODE_H
#define NODE_H

#include "Main.h"

class Node {
public:
    Node();
    Node(const Node& orig);
    virtual ~Node();
    
    long NodeNum=0;   
    double* X = NULL;
    int Dimension;
    bool NodeInitialized=false;
    
    //Support node information
    long* SupNodes = NULL;
    int SupSize=0;
    float SupRadiusSq=0;
    float SupRadius=0;
    
    //Derivative information (used for error estomation and weak form)
    double** Coeff = NULL;
    
    //Weak form additional parameters
    int WeakSupSize=0;
    long* WeakSupport = NULL;
    long* LagrangeMultCol = NULL;
    
    //Boundary node additional information
    bool BoundaryNode=false;
    bool NodeInSurfaceElement=false;
    bool* LocalBoundaryCondition = NULL;
    bool* BC_D_Bool = NULL;
    bool* BC_N_Bool = NULL;
    float* BC_D = NULL;
    float* BC_N = NULL;
    int* BC_DOF = NULL;
    float** Normal = NULL;
    
    //Exact fieald value at this node
    float* ExactU = NULL;
    float* ExactS = NULL;
    bool HasExactU=false;
    bool HasExactS=false;
    
    //Associated volume and charateristic length
    float Vol=1;
    float* SupNodeVol = NULL;
    float TotalVol=0;
    float CharactLength=0;
    float ProdN=0;
    
    //Singular Node properties
    bool IsSingular=false;
    bool HasSingularSupNodes=false;
    bool HasDiffractedSupNodes=false;
    double* DiffractedDist=NULL;
    double DistToSing=0;
    bool EnrichedNode=false;
    double* U1=NULL;
    
    //Collocation point offset
    bool CollocationPointOffset=false;
    float* CollocX = NULL;
    
    //Attached elements
    vector<long> AttachedElements;
    
    void SetNode(long, int, double, double, double);
    void SetBoundNode(bool,int,int,int,string,float, float, float, float,string,string);
    long NodeNumber();
    void SetSupRadius(float);
    void FreeCoeff();
private:
    bool BC_D_Init=false;
    bool BC_N_Init=false;
};

#endif /* NODE_H */