#ifndef GFD_PROBLEM_H
#define GFD_PROBLEM_H

#include "Model.h"

class GFD_Problem {
public:
    GFD_Problem();
    GFD_Problem(const GFD_Problem& orig);
    virtual ~GFD_Problem();
    void InitializeProblem(Model*,long*,long*);
    void LoadDerivativeCoefficients(Model*,int,int);
    void LoadVectors(Model*,double*,T*,T*,int,int);
    //Functions for enrichment
    void LoadDistToSingular(Model*);
    void LoadU1Values(Model*,int,int);
    void RefineEnrichmentZone(Model*);
private:
    MatrixXd Exponents;
    MatrixXd C_Matrix(int,Model*);
    void LoadRows2D_Stress(VectorXd,VectorXd,long,long,int,int,int,T*,long&);
    void LoadRows3D_Stress(VectorXd,long,long,int,int,int,T*,long&);
    void LoadRows2D_Stiff(Model*,VectorXd,VectorXd,long,long,int,int,int,double*,T*,long&);
    void LoadRows3D_Stiff(Model*,VectorXd,long,long,int,int,int,double*,T*,long&);
    
    float w(Model*,double);
    float factorial(int);
    float d1=0, d2=0, d3=0;
    int l=0;
    long *TripStressTreadStartInc;
    long *TripStiffTreadStartInc;
    
    //Functions for enrichment
    double Angle(Model*,long,int,long);
    double* MinU1=NULL;
    double* MaxU1=NULL;
    void GetModifiedMatricesC(Model*,long,MatrixXd&,MatrixXd&,MatrixXd&);
    void GetU1_LShape(Model*,long,MatrixXd&,MatrixXd&,MatrixXd&);
    void GetU1_CrackedPlate(Model*,long,MatrixXd&,MatrixXd&,MatrixXd&);
    void Get3DLocalNormals(float*,int,float*);
    void Get3DRotatedTerms(float*,int,float&,float,float,float,float,float,float,bool);
};

#endif /* GFD_PROBLEM_H */

