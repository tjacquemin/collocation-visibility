#ifndef VTKOUTFILE_H
#define VTKOUTFILE_H

#include "Model.h"
#include "LinearProblem.h"

class VTKOutFile {
public:
    VTKOutFile();
    VTKOutFile(const VTKOutFile& orig);
    virtual ~VTKOutFile();
    
    void PrintVTKOutputFile(Model*,LinearProblem*,string);
private:

};

#endif /* VTKOUTFILE_H */

