#ifndef LINEARPROBLEM_H
#define LINEARPROBLEM_H

#include "Model.h"
#include "GFD_Problem.h"

class LinearProblem {
public:
    LinearProblem();
    LinearProblem(const LinearProblem& orig);
    virtual ~LinearProblem();
    
    void Build(Model*);
    void LoadMatrices(Model*);
    void FillStressMat_Eigen(Model*);
    void FillLocMat_Eigen(Model*);
    
    void ComputeSolution(Model*);
    void ReorderS(Model*);
    void SetConvergenceReason(KSPConvergedReason);
    
    float d1=0, d2=0, d3=0;
    
    double* F_Vec=NULL;
    long TripStiffLength=0;
    T* TripStiff=0;
    T* TripStiffProcess=0;
    
    long TripStressLength=0;
    T* TripStress=0;
    
    long TripLocLength=0;
    T* TripLoc=NULL;
    
    long AdditionalDOFs=0; 
    long AdditionalForceVectDOFs=0;
    
    SpMat StressMat_Eigen;
    SpMat LocMat_Eigen;
    VectorXd S, U, Epsilon;    
    VectorXd S_Ord;
            
    string SolverConvergenceStatus="";
    PetscInt SolverIterationNum=0;
    string IterationStopReason_Str="";
    PetscReal ConditionNumber=0;
   
       
private:
    
    VectorXf ReducedIndex(int);
    VectorXf ExpandedIndex(int);
    VectorXf ForceVector(int);
    
    void BuildInitialization(Model*);
    void GetTripTreadStartIndices(Model*,long*,long*,long*);
    void GetWeakFormSup(Model*);
    void LoadLagrangeTriplets(Model*);
    void TestTriplets(Model*);
};

#endif /* LINEARPROBLEM_H */