#ifndef SEQ_SOLVER_H
#define SEQ_SOLVER_H

#include "Model.h"
#include "LinearProblem.h"

class Seq_Solver {
public:
    Seq_Solver();
    Seq_Solver(const Seq_Solver& orig);
    virtual ~Seq_Solver();
    
    void Solve(Model*,LinearProblem*);
    void FillStiffMat_Petsc(Model*,LinearProblem*);
    void SolveMUMPS_LU(Model*,LinearProblem*);
    void SolveKSP(Model*,LinearProblem*);
    
    Mat StiffMat;
    Vec b;
    Vec U_Petsc;
    
private:

};

#endif /* SEQ_SOLVER_H */

