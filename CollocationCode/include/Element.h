#ifndef ELEMENT_H
#define ELEMENT_H

#include "Main.h"
#include "Node.h"

class Element {
public:
    Element();
    Element(const Element& orig);
    virtual ~Element();
    
    long ElNum=0;
    int NumbNodes=0;
    int Dim=2;
    string ElementType="";
    double* Normal=NULL;
    Node** AttachedNodes=NULL;
    Node CenterNode;
    
    //Functions
    void NewElement(long,string,double*);
    void LoadConnectionsToNodes();
    void LoadCenterNode();
    
private:

};

#endif /* ELEMENT_H */

