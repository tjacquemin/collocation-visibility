#ifndef MODEL_H
#define MODEL_H

#include "Main.h"
#include "CgalLib.h"
#include "Node.h"
#include "Element.h"

class Model {
public:
    Model();
    Model(const Model& orig);
    virtual ~Model();
    
    //General Functions
    string ToString(double,int,int,bool);
    const double pi=3.14159265358979323846;
    
    //Solver and PC Information
    string SolverType="";
    string SolverPC="";
    string SolverKSP="";
    float  SolverTol=0;
    long  SolverMaxIt=0;
    float AMG_Threshold=0.02;
    int AMG_Levels=0;
    int AMG_SmoothN=0;
    int Mat_Partitioning=0;
    int Mat_Ordering=1;
    float AdjThreshold=1;
    
    int ILU_Levels=3;
    float ILU_DiagFill=0.02;
    MatOrderingType ILU_Ordering=MATORDERINGRCM;
        
    //Model Geometry
    int Dimension=0;
    float Volume=0;
    long int NodeNumber=0;
    
    //Singular Nodes
    long int SingularNodeNumber=0;
    long SingularNode=0;
    bool IncludeSingularNodes=false;
    string VisibilityType="";
    float VisibilityThresholdAngle=1;
    
    string SingularEnrichment="";
    bool FullEnrichment=true;
    double DistThreshold=-1;
    double ScaleFactor=100;
            
    //Model Input/Output Information
    string* AnalysisFile=NULL;
    string Method="";
    string ModelType="";
    string BCTreatType="";
    bool WeakBC=false;
    bool Stabilization=false;    
    bool PartialStab=false;
    string InputFilePath="";
    Node* Nodes=NULL;
    string WindowFunctionType="";
    bool ErrEstimator=false;
    bool PrintStrain=false;
    bool ExactSolutionAvailable=false;
    bool Voronoi=false;
    bool TimeSplit=false;
    string EstimatorType="";
    string EstimVar="";
    string EstimWeight="";
    int EstimSupSize=0;
    int MaxRowWidth=60;
    string FunctionBasis="";
    
    //Print options
    int PrintMat=0;
    int PrintConditionNumber=0;
    double* RunTime=NULL;
    bool PrintVTK=false;
        
    //Node Support Information
    int SupSizeInt=0;
    int SupSizeBound=0;
    int MinSupSize=0;
    int MaxSupSize=0;
    float SupRatio=0;
    float SupExp=0;
    bool SupRadiusGiven=false;
    float RadiusBoundaryNodes=0;
    float RadiusInteriorNodes=0;
    float SupScaling=1;
    string StencilSelection="";
    float StencilSelectionThreshold=-1;
    float SupInnerThreshold=0;
    Tree tree;
    
    //Mechanical Properties
    float E=0;
    float nu=0;
    
    //Processing Data
    int ThreadNumber=0;
    int ProcessNumber=0;
    
    //Main Public Functions
    string trim(string);
    string split(string, char, int);
    void LoadInputFile(string);
    void GetNodeSupports();
    void SetThreadNumber(int);
    vector<long>***  SupportGrid;
    long LoadNodeData(long,long);
    void LoadVoronoi_AllNodes();
    void LoadVoronoi();
    void ProcessMemUsage(double&, double&);
    void Print(string,bool);
    void PrintLoading(long,long,string,bool);
    void LoadSupportCGAL();
    void GetStartEndNode(int,int,long&,long&);
    
    //Surface information
    int SurfaceNumber=0;
    bool SurfaceDefined=false;
    bool InDomain(vector<float> PointVect,bool);
    Element* SurfElement=NULL;
    long ElementNumber=0;
    
    //CAD Model
    bool CAD_Model_Available=false;
    
    //Adaptivity information
    bool Adaptivity=false;
    float AdaptThreshold=0;
    float AdaptThresholdFrac=0;
    float AdaptFraction=0;
    int NumLevel=1;
    string RefType="GLOBAL";
    
    //Collocation Node Offset
    bool HasCollocationOffset=false;
    
    //Stabilization
    void ComputeCharactLength();
    
    //Error norm calculation
    double ErrorNorm(string,string,VectorXd,VectorXd,int,int);
    VectorXd ExactS;
    VectorXd ExactU;
private:
    long LoadBC_D(long,long);
    long LoadBC_N(long,long);
    long LoadBC_All(long,long);
    long LoadExactSolution(long,long);
    long LoadSurfaceVertices(long,long,int);  
    long LoadSurfaceElements(long,long,long,string);
    void LoadVoronoi_Node(long);
    long LoadSingularNodes(long,long);
    
    void LoadNormalSupport(long,long*,double*,int,int);
    void LoadAreaSupport(long,long*,double*,int,int,int);
    void LoadCoefficients();
    
    //Visibility function
    void LoadVisibilitySupport(long,long*,double*,int,int);
    void LoadConnectedElements();
    void LoadVisibilitySupport2D(long,long*,double*,int,int);
    
    void LoadTree();
    
    void GetNeighbourNodes(float,long,int*,int*,long*,double*);
    void SelectSupportNodes(long,long*,double*,bool*,int,bool*,int,int,int*,double*);
    bool IsHidden(long,long,long,double);
    bool IntersectBoundary(long,long,long);
    double GetSegmentElementAngle(MatrixXd);
    
    //Support Loading variables
    Point* BoundaryPoints=NULL;
    long SegmentsNumber=0;
            
    //Support temp variables
    long* IndexArray=NULL;
    double* DistArray=NULL;
};

#endif /* MODEL_H */