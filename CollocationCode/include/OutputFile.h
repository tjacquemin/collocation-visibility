#ifndef OUTPUTFILE_H
#define OUTPUTFILE_H

#include "Model.h"
#include "LinearProblem.h"

class OutputFile {
public:
    OutputFile();
    OutputFile(const OutputFile& orig);
    virtual ~OutputFile();
    void PrintOutputFile(Model*,LinearProblem*,string);
    
private:
    void PrintOutputFileHeader(Model*,LinearProblem*,std::ofstream*);
    void PrintErrorNorms(Model*,LinearProblem*,std::ofstream*);
    void PrintApproxSolution(Model*,LinearProblem*,std::ofstream*);
    void PrintEstimExactSolution(Model*,LinearProblem*,std::ofstream*);
    
    void CalculateErrorNorms(Model*,LinearProblem*,std::ofstream*);
    void CalculatePrintExactErrorS(Model*,LinearProblem*,std::ofstream*);
    void CalculatePrintExactErrorU(Model*,LinearProblem*,std::ofstream*);
        
    float GetAverageBCNodeSupport(Model*);
    float GetAverageInnerNodeSupport(Model*);
    VectorXd L1_S, L1_U, L2_S, L2_SR, L2_U, LInf_S, LInf_U;
};

#endif /* OUTPUTFILE_H */