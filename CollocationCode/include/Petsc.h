#ifndef PETSC_H
#define PETSC_H

#include "petscmat.h" 
#include "petscvec.h" 
#include "petscksp.h" 
#include <petscts.h>
#include <petscdraw.h>
 #include <petscdmda.h>

#endif /* PETSC_H */

