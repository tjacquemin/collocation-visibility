#include "DoxygenDoc.h"
#include "Model.h"
#include "LinearProblem.h"
#include "OutputFile.h"
#include "VTKOutFile.h"
#include "Seq_Solver.h"

int main(int argc, char** argv)
{
    int rank, size;
    PetscInitialize(&argc,&argv,PETSC_NULL, PETSC_NULL);
    
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    //Create the model class
    Model* MainModel=NULL;
    
    //Create the linear problem
    LinearProblem* Problem=NULL;
    
    //Variables on all processes
    string FilePath;
    char* PathChar;
    double StartTime, RunTime, IntermediateTime, EndTime;
    //Run the preprocessing using multithreading on the master node
    if (rank==0)
    {
        //Create the model on the master node
        MainModel = new Model;
        MainModel->ProcessNumber=size;
        
        //Program header
        cout << " *******************************************\n";
        cout << " *                                         *\n";
        cout << " *               Legato Team               *\n";
        cout << " *          Luxembourg University          *\n";
        cout << " *                                         *\n";
        cout << " *        Meshless Collocation Code        *\n";
        cout << " *            Thibault JACQUEMIN           *\n";
        cout << " *                2016 - 2021              *\n";
        cout << " *                                         *\n";
        cout << " *******************************************\n";
        
        StartTime=omp_get_wtime();
        MainModel->RunTime = new double[5];
        
        PathChar=argv[1];
        if (PathChar == NULL)
        {
            FilePath="/home/tjacquemin/PhD_Linux/Calculations/2D/2D_Problems/GearKey_Vis/CylinderTest/2D_Cylinder_01_Global_0.060-c.inp";
        }
        else
        {
            FilePath=string(PathChar);
        }
        
        cout << "" << endl;
        MainModel->Print("Running: " + FilePath,false);
        time_t TimeAtStart = time(0);
        
        cout << " Start Time: " << ctime(&TimeAtStart) << endl;
        cout << "" << endl;
        cout << " *******************************************" << endl;
        cout << " *              Preprocessing              *" << endl;
        cout << " *******************************************" << endl;
        cout << "" << endl;
        
        //Load the analysis file and create the model
        MainModel->LoadInputFile(FilePath);
        MainModel->Print("Model loaded from the input file.",true);
        
        //Set the number of threads
        MainModel->SetThreadNumber(MainModel->ThreadNumber);
        MainModel->Print("Loading the node supports.",true);
        
        //Algorthim from CGAL library
        MainModel->LoadSupportCGAL();
                
        //Create the linear problem on the master node
        Problem = new LinearProblem;
        
        //Getting the time spent for preliminary tasks
        IntermediateTime=omp_get_wtime();
        MainModel->RunTime[0] = (double)(IntermediateTime-StartTime) / ((double)CLOCKS_PER_SEC);
        
        //Build the problem based on the defined model on one process using mutithread
        MainModel->Print("Building the Problem.",true);
        Problem->Build(MainModel);
        
        //Saving the time spent to build the system
        RunTime=omp_get_wtime()-IntermediateTime;
        
        MainModel->RunTime[1] = (double)RunTime / ((double)CLOCKS_PER_SEC);
        MainModel->Print("Model Successfully Built.",true);
        IntermediateTime=omp_get_wtime();
        
        Problem->LoadMatrices(MainModel); 
    }
    if (rank==0)
    {
        cout << "" << endl;
        cout << " *******************************************" << endl;
        cout << " *               Calculation               *" << endl;
        cout << " *******************************************" << endl;
        cout << "" << endl;
    }
    
    if (rank==0)
    {
        if (size==1)
        {
            //Solve the system using the Sequential solver
            Seq_Solver Seq_Solve;           
            Seq_Solve.Solve(MainModel,Problem);
        }
        MainModel->Print(Problem->IterationStopReason_Str,true);
        
        RunTime=omp_get_wtime()-IntermediateTime;
        MainModel->RunTime[2] = RunTime;
        MainModel->RunTime[3] = omp_get_wtime()-StartTime;
        
        if (rank==0)
        {
            cout << "" << endl;
            cout << " *******************************************" << endl;
            cout << " *             Postprocessing              *" << endl;
            cout << " *******************************************" << endl;
            cout << "" << endl;
        }
        EndTime=omp_get_wtime();
        Problem->ComputeSolution(MainModel);
                
        //Print the solution to an output file
        OutputFile* Output=NULL;
        Output=new OutputFile;
        Output->PrintOutputFile(MainModel,Problem,FilePath);
        MainModel->Print("Results saved to the .out file.",true);
        
        //Print VTK output file if requested
        if (MainModel->PrintVTK==true)
        {
            VTKOutFile* VTKFile=NULL;
            VTKFile= new VTKOutFile;
            VTKFile->PrintVTKOutputFile(MainModel,Problem,FilePath);
        }
        
        //Postprocessing Time
        MainModel->RunTime[4]=omp_get_wtime()-EndTime;
        
        if (MainModel->TimeSplit==true)
        {
            cout << " Prep. Runtime=" << MainModel->RunTime[0] << "s" << endl;
            cout << " Buil. Runtime=" << MainModel->RunTime[1] << "s" << endl;
            cout << " Solv. Runtime=" << MainModel->RunTime[2] << "s" << endl;
            cout << " PPro. Runtime=" << MainModel->RunTime[4] << "s" << endl;
        }
        cout << " Total Runtime=" << MainModel->RunTime[3] << "s" << endl;
        cout << " ----------------------------------------------------------------------" << endl ;
        cout << " " << endl;
        
        delete MainModel;
        delete Problem;
    }
    
    PetscFinalize();
    return 0;
}