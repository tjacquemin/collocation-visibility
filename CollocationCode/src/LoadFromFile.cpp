#include "Model.h"
void Model::LoadInputFile(string Value)
{
    string str;
    long Count=0;
    char Path[Value.length()+1];
    strncpy(Path, Value.c_str(),Value.length()+1);
    InputFilePath=Value;
    ifstream File;
    //Open the selected file
    File.open (Path, ios::in);
    //Add all the lines of the file in a vector
    while (getline(File, str))
    { 
        if (str!="")
           Count++;
    }
    File.close();
    AnalysisFile = new string[Count];
    File.open (Path, ios::in);
    //Add all the lines of the file in a vector
    long i=0;
    while (getline(File, str))
    {
        if (str!="")
        {
            AnalysisFile[i]=str;
            i++;
        }
    }
    File.close();
    i = 0;
    //Read all the lines of the input file vector to find the identifiers
    //For each identifier, load the input data to the associated variables
    while (i <= Count-1)
    {
        if (AnalysisFile[i].substr(0,1) == "#")
        {
            if ( split(AnalysisFile[i],'=',0)=="#ANALYSIS_TYPE")
            {
                ModelType=split(AnalysisFile[i],'=',1);
                if (ModelType=="2D_PLANE_STRESS" || ModelType=="2D_PLANE_STRAIN")
                    Dimension=2;
                else if (ModelType=="3D_ELASTICITY")
                    Dimension=3;
            }
            else if (split(AnalysisFile[i],'=',0)=="#PARAMETERS")
            {
                i++;
                E=stof(split(AnalysisFile[i],'=',1));
                i++;
                nu=stof(split(AnalysisFile[i],'=',1));
            }
            else if ( split(AnalysisFile[i],'=',0)=="#METHOD")
            {
                Method=split(AnalysisFile[i],'=',1);
            }
            else if ( split(AnalysisFile[i],'=',0)=="#BASIS")
            {
                FunctionBasis=split(AnalysisFile[i],'=',1);
            }
            else if ( split(AnalysisFile[i],'=',0)=="#STABILIZATION")
            {
                if (split(AnalysisFile[i],'=',1)=="TRUE")
                {
                    Stabilization=true;
                }
                if (split(AnalysisFile[i],'=',1)=="PARTIAL")
                {
                    Stabilization=true;
                    PartialStab=true;
                }
            }
            else if ( split(AnalysisFile[i],'=',0)=="#BC_TREATMENT")
            {
                BCTreatType=split(AnalysisFile[i],'=',1);
            }
            else if ( split(AnalysisFile[i],'=',0)=="#WEAK_BC")
            {
                if (split(AnalysisFile[i],'=',1)=="TRUE") 
                    WeakBC=true;
            }
            else if ( split(AnalysisFile[i],'=',0)=="#WINDOW_TYPE")
            {
                WindowFunctionType=split(split(AnalysisFile[i],'=',1),',',0);
                SupRatio=stof(split(split(AnalysisFile[i],'=',1),',',1));
                SupExp=SupRatio;
            }
            else if ( split(AnalysisFile[i],'=',0)=="#VORONOI")
            {
                if (split(split(AnalysisFile[i],'=',1),',',0)=="TRUE")
                    Voronoi=true;
            }
            else if ( split(AnalysisFile[i],'=',0)=="#ERR_ESTIMATOR")
            {
                if (split(split(AnalysisFile[i],'=',1),',',0)=="TRUE")
                {
                    ErrEstimator=true;
                    EstimatorType=split(split(AnalysisFile[i],'=',1),',',1);
                    if (EstimatorType=="GR")
                    {
                        EstimVar=split(split(AnalysisFile[i],'=',1),',',2);
                    }
                }
            }
            else if (split(AnalysisFile[i],'=',0)=="#ERR_ESTIMATOR_WEIGHT")
            {
                EstimWeight=split(AnalysisFile[i],'=',1);
            }
            else if (split(AnalysisFile[i],'=',0)=="#ERR_ESTIMATOR_SUP_SIZE")
            {
                EstimSupSize=stoi(split(AnalysisFile[i],'=',1));
            }
            else if (split(AnalysisFile[i],'=',0) == "#SUP_SIZE")
            {
                SupRadiusGiven=false;
                SupSizeInt = stoi(split(split(AnalysisFile[i],'=',1),',',0));
                SupSizeBound=stoi(split(split(AnalysisFile[i],'=',1),',',1));
            }
            else if (split(AnalysisFile[i],'=',0) == "#SUP_INNER_THRESHOLD")
            {
                SupInnerThreshold=stof(split(AnalysisFile[i],'=',1));
            }
            else if (split(AnalysisFile[i],'=',0) == "#STENCIL_SEL")
            {
                StencilSelection=split(split(AnalysisFile[i],'=',1),',',0);
                StencilSelectionThreshold=stof(split(split(AnalysisFile[i],'=',1),',',1));
            }
            else if (split(AnalysisFile[i],'=',0) == "#SUP_SCALING")
            {
                SupScaling=stof(split(AnalysisFile[i],'=',1));
            }
            else if (split(AnalysisFile[i],'=',0) == "#SUP_RADIUS")
            {
                SupRadiusGiven=true;
                RadiusInteriorNodes=stof(split(split(AnalysisFile[i],'=',1),',',0));
                RadiusBoundaryNodes= stof(split(split(AnalysisFile[i],'=',1),',',1));
            }
            else if (split(AnalysisFile[i],'=',0) == "#VOLUME")
            {
                Volume = stof(split(AnalysisFile[i],'=',1));
            }
            else if (split(AnalysisFile[i],'=',0) == "#SOLVER")
            {
                SolverType = split(AnalysisFile[i],'=',1);
            }
            else if (split(AnalysisFile[i],'=',0) == "#SOLVER_SPEC")
            {
                SolverPC = split(split(AnalysisFile[i],'=',1),',',0);
                SolverKSP = split(split(AnalysisFile[i],'=',1),',',1);
            }
            else if (split(AnalysisFile[i],'=',0) == "#SOLVER_TOL")
            {
                SolverTol = stof(split(split(AnalysisFile[i],'=',1),',',0));
                SolverMaxIt=stol(split(split(AnalysisFile[i],'=',1),',',1));
            }
            else if (split(AnalysisFile[i],'=',0) == "#AMG_SPEC")
            {
                AMG_Threshold = stof(split(split(AnalysisFile[i],'=',1),',',0));
                AMG_Levels=stoi(split(split(AnalysisFile[i],'=',1),',',1));
                AMG_SmoothN=stoi(split(split(AnalysisFile[i],'=',1),',',2));
            }
            else if (split(AnalysisFile[i],'=',0) == "#ILU_SPEC")
            {
                ILU_Levels = stoi(split(split(AnalysisFile[i],'=',1),',',0));
                ILU_DiagFill=stof(split(split(AnalysisFile[i],'=',1),',',1));
            }
            else if (split(AnalysisFile[i],'=',0) == "#MAT_ORDERING")
            {
                if(split(AnalysisFile[i],'=',1)=="FALSE")
                    Mat_Ordering = 0;
            }
            else if (split(AnalysisFile[i],'=',0) == "#MAT_PARTITIONING")
            {
                if(split(AnalysisFile[i],'=',1)=="TRUE")
                    Mat_Partitioning = 1;
            }
            else if (split(AnalysisFile[i],'=',0) == "#ADJ_TRESHOLD")
            {
                AdjThreshold=stof(split(AnalysisFile[i],'=',1));
            }
            else if (split(AnalysisFile[i],'=',0) == "#INCLUDE_SINGULAR")
            {
                if (split(AnalysisFile[i],'=',1) == "TRUE")
                    IncludeSingularNodes=true;
            }
            else if (split(AnalysisFile[i],'=',0) == "#SINGULAR_NODES")
            {
                SingularNodeNumber=stol(split(AnalysisFile[i],'=',1));
                i=LoadSingularNodes(i,Count);
            }
            else if (split(AnalysisFile[i],'=',0) == "#SINGULAR_ENRICHEMENT")
            {
                SingularEnrichment=split(split(AnalysisFile[i],'=',1),',',0);
                if (split(split(AnalysisFile[i],'=',1),',',1)=="TRUE")
                {
                    FullEnrichment=true;
                }
                else
                {
                    FullEnrichment=false;
                }
            }
            else if (split(AnalysisFile[i],'=',0) == "#SINGULAR_THRESHOLD")
            {
                DistThreshold=stod(split(split(AnalysisFile[i],'=',1),',',0));
                ScaleFactor=stod(split(split(AnalysisFile[i],'=',1),',',1));
            }
            else if (split(AnalysisFile[i],'=',0) == "#VISIBILITY_TYPE")
            {
                VisibilityType=split(AnalysisFile[i],'=',1);
            }
            else if (split(AnalysisFile[i],'=',0) == "#VISIBILITY_THRESHOLD")
            {
                VisibilityThresholdAngle=stof(split(AnalysisFile[i],'=',1));
            }
            else if (trim(AnalysisFile[i]) == "#NODE_DATA")
            {
                i=LoadNodeData(i,Count);
            }
            else if (trim(AnalysisFile[i]) == "#BOUNDARY_DIRICHLET")
            {
                i=LoadBC_D(i,Count);
            }
            else if (trim(AnalysisFile[i]) == "#BOUNDARY_NEUMANN")
            {
                i=LoadBC_N(i,Count);
            }
            else if (trim(AnalysisFile[i]) == "#BOUNDARY")
            {
                i=LoadBC_All(i,Count);
            }
            else if (trim(AnalysisFile[i]) == "#EXACT_SOLUTION")
            {
                ExactSolutionAvailable=true;
                i=LoadExactSolution(i,Count);
            }
            else if (split(AnalysisFile[i],'=',0) == "#SURFACE_EL")
            {
                int SurfNumber=stoi(split(split(AnalysisFile[i],'=',1),',',0));
                ElementNumber=stol(split(split(AnalysisFile[i],'=',1),',',1));
                string ElType=split(split(AnalysisFile[i],'=',1),',',2);
                i=LoadSurfaceElements(i,Count,ElementNumber,ElType);
            }
            else if (split(AnalysisFile[i],'=',0) == "#ADAPTIVITY")
            {
                if (trim(split(AnalysisFile[i],'=',1))=="TRUE")
                    Adaptivity=true;
                for (int j=0; j<4; j++)
                {
                    i++;
                    if (split(AnalysisFile[i],'=',0)=="THRESHOLD")
                        AdaptThreshold=stof(split(AnalysisFile[i],'=',1));
                    else if (split(AnalysisFile[i],'=',0)=="THRESHOLD_FRAC")
                        AdaptThresholdFrac=stof(split(AnalysisFile[i],'=',1));
                    else if (split(AnalysisFile[i],'=',0)=="MAX_ADAPT_FRAC")
                        AdaptFraction=stof(split(AnalysisFile[i],'=',1));
                    else if (split(AnalysisFile[i],'=',0)=="LEVEL")
                    {
                        NumLevel=stoi(split(AnalysisFile[i],'=',1));
                        NumLevel++;
                    }
                    else if (split(AnalysisFile[i],'=',0)=="REFINEMENT_TYPE")
                    {
                        RefType=split(AnalysisFile[i],'=',1);
                    }
                }
            }
            else if (split(AnalysisFile[i],'=',0) == "#HAS_COLLOC_OFFSET")
            {
                if (trim(split(AnalysisFile[i],'=',1))=="TRUE")
                    HasCollocationOffset=true;
            }
            else if (split(AnalysisFile[i],'=',0) == "#THREAD")
            {
                ThreadNumber=stoi(split(AnalysisFile[i],'=',1));
            }
            else if (split(AnalysisFile[i],'=',0) == "#PRINT_OPTIONS")
            {
                while (AnalysisFile[i+1].substr(0,1)  != "#")
                {
                    i++;
                    if (split(AnalysisFile[i],'=',0) == "VIEW_MAT")
                    {
                        if (split(AnalysisFile[i],'=',1)=="TRUE")
                            PrintMat=1;
                    }
                    else if (split(AnalysisFile[i],'=',0) == "CONDITION_NUM")
                    {
                        if (split(AnalysisFile[i],'=',1) =="TRUE")
                            PrintConditionNumber=1;
                        else if (split(AnalysisFile[i],'=',1) =="FALSE")
                            PrintConditionNumber=0;
                    }
                    else if (split(AnalysisFile[i],'=',0) == "TIME_SPLIT")
                    {
                        if(split(AnalysisFile[i],'=',1)=="TRUE")
                            TimeSplit = true;
                    }
                    else if (split(AnalysisFile[i],'=',0) == "PRINT_STRAIN")
                    {
                        if(split(AnalysisFile[i],'=',1)=="TRUE")
                            PrintStrain = true;
                    }
                    else if (split(AnalysisFile[i],'=',0) == "PRINT_VTK")
                    {
                        if(split(AnalysisFile[i],'=',1)=="TRUE")
                            PrintVTK = true;
                    }
                }
                /** \page PagePrintOptions Print Options
                 *   Additional print options can be added to the output file with the keyword \b #PRINT_OPTIONS. \n
                 *   - \b #PRINT_OPTIONS \n
                 *    *VIEW_MAT=TRUE* \n
                 *    *CONDITION_NUM=TRUE* \n
                 *    *TIME_SPLIT=TRUE* \n
                 *    *PRINT_STRAIN=TRUE* \n
                 *   
                */
            }
            else if (trim(AnalysisFile[i]) == "#END")
            {
                i=Count+10;
            }
        }
        i++;
    }
    //Keep the file in the memory for the adaptivity section
    if (Adaptivity==false)
    {
        delete[] AnalysisFile;
    }
}

long Model::LoadNodeData(long FirstRow, long FileEnd)
{   
    long i=FirstRow+1;
    long Count=0;
    double X1, X2, X3=0;
    long NodeNum;
    //Count the number of nodes
    while (i <= FileEnd && AnalysisFile[i].substr(0,1)  != "#")
    {
        if (AnalysisFile[i].empty()==false)
        {
            Count++;
        }
        i++;
    }
    i=FirstRow+1;
    //Create the array of nodes
    Nodes = new Node[Count+1];
    NodeNumber=Count;
    //Once the node vector of the appropriate dimension, fill it with the node data
    while (i <= FileEnd)
    {
        if (AnalysisFile[i].empty()==false)
        {
            NodeNum=stol(split(AnalysisFile[i],',',0));
            X1=stod(split(AnalysisFile[i],',',1));
            X2=stod(split(AnalysisFile[i],',',2));
            if (Dimension==3)
            {
                X3=stod(split(AnalysisFile[i],',',3));
            }
            if (HasCollocationOffset==true)
            {
                if (split(AnalysisFile[i],',',Dimension+1)!="")
                {
                    Nodes[NodeNum].CollocationPointOffset=true;
                    Nodes[NodeNum].CollocX = new float[Dimension];
                    for (int j=0; j<Dimension; j++)
                    {
                        Nodes[NodeNum].CollocX[j]=stod(split(AnalysisFile[i],',',Dimension+1+j));
                    }
                }
            }
            Nodes[NodeNum].SetNode(NodeNum, Dimension, X1, X2, X3);
        }
        i++;
        //Exit the loop if a new identifer is found or if the end of the file is reached
        if (AnalysisFile[i].substr(0,1)  == "#")
        {
            break;
        }
    }
    return i-1;
}

long Model::LoadBC_D(long FirstRow, long FileEnd)
{
    long i=FirstRow+1;
    float Value;
    long NodeNum;
    int DOF;
    //Once the BC vector of the appropriate dimension, fill it with the node data
    while (i <= FileEnd-1)
    {
        if (AnalysisFile[i].empty()==false)
        {
            NodeNum=stol(split(AnalysisFile[i],',',0));
            DOF=stoi(split(AnalysisFile[i],',',1));
            Value=stod(split(AnalysisFile[i],',',2)); 
            Nodes[NodeNum].SetBoundNode(true,Dimension,DOF,DOF-1,"*",Value,0,0,0,"D","G");
        }
        i++;
        //Exit the loop if a new identifer is found or if the end of the file is reached
        if (AnalysisFile[i].substr(0,1)  == "#" || i == FileEnd-1)
            break;
    }
    return i-1;
}

long Model::LoadBC_N(long FirstRow, long FileEnd)
{
    long i=FirstRow+1;
    float Value,NX1,NX2;
    float NX3=0;
    long NodeNum;
    int DOF;
    i=FirstRow+1;
    //Once the BC vector of the appropriate dimension, fill it with the node data
    while (i <= FileEnd-1)
    {
        if (AnalysisFile[i].empty()==false)
        {
            NodeNum=stol(split(trim(AnalysisFile[i]),',',0));
            NX1=stod(split(AnalysisFile[i],',',1));
            NX2=stod(split(AnalysisFile[i],',',2));
            int inc=3;
            if (Dimension==3)
            {
                NX3=stod(trim(split(AnalysisFile[i],',',3)));
                inc=4;
            }
            string DOF_Val="";
            DOF_Val=split(AnalysisFile[i],',',inc);
            while (DOF_Val != "")
            {
                DOF=stoi(split(AnalysisFile[i],',',inc));
                Value=stod(split(AnalysisFile[i],',',inc+1));
                Nodes[NodeNum].SetBoundNode(true,Dimension,DOF,DOF-1,"*",Value,NX1,NX2,NX3,"N","G");
                inc=inc+2;
                DOF_Val=split(AnalysisFile[i],',',inc);
            }
        }
        i++;
        //Exit the loop if a new identifer is found or if the end of the file is reached
        if (i == FileEnd-1)
            break;
        if (AnalysisFile[i].substr(0,1)  == "#" || i == FileEnd)
            break;
    }
    return i-1;
}

long Model::LoadBC_All(long FirstRow, long FileEnd)
{
    long i=FirstRow+1;
    float NX1,NX2;
    float NX3=0;
    long NodeNum;
    string BC_Type="";
    string BC_Axis="";
    double BC_Val=0;
    i=FirstRow+1;
    //Once the BC vector of the appropriate dimension, fill it with the node data
    while (i <= FileEnd-1)
    {
        if (AnalysisFile[i].empty()==false)
        {
            int BCNodeBlockSize=1;
            int NumBCPerRow=1;
            int BCDOF=-1;
            int BCIndex=-1;
            string BCDOFStr=split(AnalysisFile[i],',',1);
            if (BCDOFStr!="*")
            {
                BCNodeBlockSize=Dimension;
                NumBCPerRow=1;
            }
            else
            {
                BCNodeBlockSize=1;
                NumBCPerRow=Dimension;
            }
            for (int ii=0;ii<BCNodeBlockSize;ii++)
            {
                BCDOFStr=split(AnalysisFile[i],',',1);
                if (BCDOFStr!="*")
                    BCDOF=stoi(BCDOFStr)-1;
                NodeNum=stol(split(trim(AnalysisFile[i]),',',0));
                NX1=stof(split(AnalysisFile[i],',',2));
                NX2=stof(split(AnalysisFile[i],',',3));
                if (Dimension==3)
                {
                    NX3=stof(trim(split(AnalysisFile[i],',',4)));
                }
                for (int j=0;j<NumBCPerRow;j++)
                {
                    if (BCDOFStr=="*")
                    {
                        BCDOF=j;
                    }
                    BC_Type=split(AnalysisFile[i],',',Dimension+2+3*j);
                    BC_Axis=split(AnalysisFile[i],',',Dimension+3+3*j);
                    BC_Val=stof(split(AnalysisFile[i],',',Dimension+4+3*j));
                    BCIndex=j;
                    if (ii>j)
                        BCIndex=ii;
                    if (BC_Type=="D")
                        Nodes[NodeNum].SetBoundNode(true,Dimension,BCDOF,BCIndex,BCDOFStr,BC_Val,NX1,NX2,NX3,"D",BC_Axis);
                    else if (BC_Type=="N")
                        Nodes[NodeNum].SetBoundNode(true,Dimension,BCDOF,BCIndex,BCDOFStr,BC_Val,NX1,NX2,NX3,"N",BC_Axis);                        
                }
                if (BCNodeBlockSize>1 && ii<BCNodeBlockSize-1)
                    i++;
            }
        }
        i++;
        
        //Exit the loop if a new identifer is found or if the end of the file is reached
        if (i == FileEnd-1)
            break;
        if (AnalysisFile[i].substr(0,1)  == "#" || i == FileEnd)
            break;
    }
    return i-1;
}

long Model::LoadExactSolution(long FirstRow, long FileEnd)
{
    long i=FirstRow+1;
    string ExactValue;
    int StressLastIndex=5;
    long NodeNum;
    if (Dimension==3)
    {
        StressLastIndex=9;
    }
    while (i <= FileEnd-1)
    {
        if (AnalysisFile[i].empty()==false)
        {
            NodeNum=stol(split(trim(AnalysisFile[i]),',',0));
            for (int j=1; j<=Dimension; j++)
            {
                ExactValue=trim(split(AnalysisFile[i],',',j));
                if (ExactValue != "~")
                {
                    if (j==1)
                    {
                        Nodes[NodeNum].ExactU=new float[Dimension];
                        Nodes[NodeNum].HasExactU=true;
                    }
                    Nodes[NodeNum].ExactU[j-1]=stof(ExactValue);
                }
            }
            for (int j=Dimension+1; j<=StressLastIndex; j++)
            {
                ExactValue=split(AnalysisFile[i],',',j);
                if (ExactValue != "~")
                {
                    if (j==Dimension+1)
                    {
                        Nodes[NodeNum].ExactS=new float[3*(Dimension-1)];
                        Nodes[NodeNum].HasExactS=true;
                    }
                    if (trim(ExactValue) != "Inf")
                    {
                        Nodes[NodeNum].ExactS[j-Dimension-1]=stof(ExactValue);
                    }
                    else
                    {
                        Nodes[NodeNum].ExactS[j-Dimension-1]=9.9999E30;
                    }
                }
            }
        }
        i++;
        
        //Exit the loop if a new identifer is found or if the end of the file is reached
        if (i == FileEnd)
        {
            break;
        }
        else if (AnalysisFile[i].substr(0,1)  == "#")
        {
            break;
        }
    }
    return i-1;
}

long Model::LoadSurfaceElements(long FirstRow, long FileEnd, long NumElements, string ElType)
{
    //Create the element Array
    SurfElement= new Element[NumElements];
    long i=FirstRow+1;
    long ElNum=0;
    long ElCount=0;
    double* ElNomal=NULL;
    ElNomal=new double[Dimension];
    long LocNodeNum=0;
    while (i <= FileEnd-1)
    {
        if (AnalysisFile[i].empty()==false)
        {
            //Get the element number
            ElNum=stol(split(AnalysisFile[i],',',0));
            //Get the element normal
            if (ElType=="2D_LINEAR" || ElType=="2D_QUAD" || ElType=="3D_LINEAR")
            {
                for (int j=0; j<Dimension; j++)
                {
                    ElNomal[j]=stod(split(AnalysisFile[i],',',j+Dimension+1));
                }
            }
            //Create the element and load the element type and the normal
            SurfElement[ElNum-1].NewElement(ElNum,ElType,ElNomal);
            //Load the nodes connected to the element
            if (ElType=="2D_LINEAR" || ElType=="2D_QUAD" || ElType=="3D_LINEAR")
            {
                for (int j=0; j<Dimension; j++)
                {
                    LocNodeNum=stol(split(AnalysisFile[i],',',j+1));
                    Nodes[LocNodeNum].NodeInSurfaceElement=true;
                    SurfElement[ElNum-1].AttachedNodes[j]=&Nodes[LocNodeNum];                    
                }
                if (ElType=="2D_QUAD")
                {
                    SurfElement[ElNum-1].CenterNode.SetNode(0,Dimension,stod(split(AnalysisFile[i],',',5)),stod(split(AnalysisFile[i],',',6)),0);
                }
            }
            
            ElCount++;
        }
        i++;
        //Exit the loop if a new identifer is found or if the end of the file is reached
        if (AnalysisFile[i].substr(0,1)  == "#")
        {
            break;
        }
    }
    delete[] ElNomal;
    return i-1;
}

long Model::LoadSingularNodes(long FirstRow, long FileEnd)
{
    long i=FirstRow+1;
    float NX1,NX2;
    float NX3=0;
    long NodeNum;
    
    //Once the BC vector of the appropriate dimension, fill it with the node data
    while (i <= FileEnd-1)
    {
        if (AnalysisFile[i].empty()==false)
        {
            NodeNum=stol(split(trim(AnalysisFile[i]),',',0));
            NX1=stof(split(AnalysisFile[i],',',1));
            NX2=stof(split(AnalysisFile[i],',',2));
            if (Dimension==3)
            {
                NX3=stof(trim(split(AnalysisFile[i],',',3)));
            }
            Nodes[NodeNum].IsSingular=true;
            Nodes[NodeNum].SetBoundNode(false,Dimension,0,0,"*",0,NX1,NX2,NX3,"N","G");
            SingularNode=NodeNum;
        }
        i++;
        //Exit the loop if a new identifer is found or if the end of the file is reached
        if (AnalysisFile[i].substr(0,1)  == "#" || i == FileEnd-1)
            break;
    }
    if (IncludeSingularNodes==false)
    {
        NodeNumber-=SingularNodeNumber;
    }
    return i-1;
}