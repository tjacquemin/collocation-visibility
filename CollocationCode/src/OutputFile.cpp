#include "OutputFile.h"

OutputFile::OutputFile() {
}

OutputFile::OutputFile(const OutputFile& orig) {
}

OutputFile::~OutputFile() {
}
void OutputFile::PrintOutputFile(Model* M, LinearProblem* L, string Path)
{
    std::ofstream *outfile;
    outfile= new std::ofstream(Path.replace(Path.end()-3,Path.end(),"out"));
        
    PrintOutputFileHeader(M,L,outfile);
    PrintErrorNorms(M,L,outfile);
    PrintApproxSolution(M,L,outfile);
    PrintEstimExactSolution(M,L,outfile);
        
    outfile->close();
    delete outfile;
}

void OutputFile::PrintOutputFileHeader(Model* M, LinearProblem* L, std::ofstream* outfile)
{
    time_t now = time(0);
    *outfile << "********************************" << endl;
    *outfile << "**     COLLOCATION v1.0.9     **" << endl;
    *outfile << "**            ----            **" << endl;
    *outfile << "**      by  T. JACQUEMIN      **" << endl;
    *outfile << "**         Legato Team        **" << endl;
    *outfile << "**  University of Luxembourg  **" << endl;
    *outfile << "********************************" << endl;
    *outfile << "" << endl;
    *outfile << "********************************" << endl;
    *outfile << "**     ANALYSIS COMPLETED     **" << endl;
    *outfile << "********************************" << endl;
    *outfile << ctime(&now) << endl;
    
    *outfile << "#PROCESSES_USED=" << M->ProcessNumber << endl;
    *outfile << "#THREAD_USED=" << M->ThreadNumber << endl;
    *outfile << "#RUNTIME=" << M->RunTime[3] << endl;
    if (M->TimeSplit==true)
    {
        *outfile << " PREP.=" << M->RunTime[0] << endl;
        *outfile << " BUIL.=" << M->RunTime[1] << endl;
        *outfile << " SOLV.=" << M->RunTime[2] << endl;
        *outfile << " PPRO.=" << M->RunTime[4] << endl;
    }
    
    *outfile << "" << endl;
    *outfile << "    PROBLEM INFORMATION:" << endl;
    *outfile << "    ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾" << endl;
    *outfile << "#ANALYSIS_TYPE=" << M->ModelType << endl;
    *outfile << "#DIMENSION=" << M->Dimension << endl;
    *outfile << "#NODE_NUMBER=" << M->NodeNumber << endl;
    
    *outfile << "" << endl;
    *outfile << "    METHOD INFORMATION:" << endl;
    *outfile << "    ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾" << endl;
    *outfile << "#METHOD=" << M->Method << endl;
    *outfile << "#WINDOW_TYPE=" << M->WindowFunctionType << "," << M->SupRatio << endl;
    if (M->Method == "DCPSE0" || M->Method == "DCPSE1" || M->Method == "DCPSE2") {
        *outfile << "#COR_FUNCT_TYPE=" << M->FunctionBasis << endl;
    }
    
    *outfile << "#STABILIZATION=";
    if (M->Stabilization == true) {
        *outfile << "TRUE" << endl;}
    else{
        *outfile << "FALSE" << endl;}
    if (M->VisibilityType != "")
    {
        *outfile << "#VISIBILITY_TYPE=" << M->VisibilityType << endl;
        *outfile << "#VISIBILITY_THRESHOLD=" << M->VisibilityThresholdAngle << endl;
    }
    if (M->BCTreatType != "") *outfile << "#BC_TYPE=" << M->BCTreatType << endl;
    *outfile << "#REQ_SUP_SIZE=" << M->SupSizeInt <<"," << M->SupSizeBound << endl;
    *outfile << "#AVERAGE_INNER_SUP=" << GetAverageInnerNodeSupport(M) << endl;
    *outfile << "#AVERAGE_BC_SUP=" << GetAverageBCNodeSupport(M) << endl;
    
    *outfile << "" << endl;
    *outfile << "    SOLVER INFORMATION:" << endl;
    *outfile << "    ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾" << endl;
    *outfile << "#SOLVER_TYPE=" << M->SolverType << endl;
    
    //Print additional informations for the KSP solver
    if (M->SolverType=="KSP")
    {
        *outfile << "#SOLVER_SPEC=" << M->SolverPC << "," << M->SolverKSP << endl;
        if (M->SolverPC=="GAMG" || M->SolverPC=="BOOMERAMG" )
        {
            *outfile << "#PC_AMG_SPEC=" << M->AMG_Threshold;
            if (M->AMG_Levels>0) *outfile << "," << M->AMG_Levels;
            else  *outfile << ",default";
            if (M->AMG_SmoothN>0) *outfile << "," << M->AMG_SmoothN;
            else  *outfile << ",default";
            *outfile << endl;
        }
        *outfile << "#SOLVER_TOL=" << M->SolverTol << endl;
        *outfile << "#SOLVER_ADDITIONAL_INFORMATION" << endl;
        *outfile << " " << L->SolverConvergenceStatus << endl;
        *outfile << "#SOLVER_ITERATION_NUMBER=" << L->SolverIterationNum << endl;
        *outfile << "#ITERATION_STOP_REASON"  << endl;
        *outfile << " " << L->IterationStopReason_Str << endl;
    }
    
    *outfile << "" << endl;
    *outfile << "    SOLUTION INFORMATION:" << endl;
    *outfile << "    ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾" << endl;
    if(M->PrintConditionNumber==1)
    {
        *outfile << "#CONDITION_NUMBER=" << L->ConditionNumber << endl;
    }
}

void OutputFile::PrintErrorNorms(Model* M, LinearProblem* L, std::ofstream* outfile)
{
    //Calculate and print the exact error norms is the solution is available
    if (M->ExactSolutionAvailable==true)
    {
        CalculateErrorNorms(M,L,outfile);
    }
}

void OutputFile::PrintApproxSolution(Model* M, LinearProblem* L, std::ofstream* outfile)
{
    int Dim=M->Dimension;
    string Line;
    *outfile << "" << endl;
    *outfile << "    SOLUTION:" << endl;
    *outfile << "    ‾‾‾‾‾‾‾‾" << endl;
    *outfile << "#APPROXIMATED_SOLUTION" << endl;
    if (Dim==2)
    {
        *outfile << " NodeNum   X1             X2             U1             U2             S11            S12            S22" << std::endl;
    }
    else if (Dim==3)
    {
        *outfile << " NodeNum   X1             X2             X3             U1             U2             U3             S11            S12            S13            S22            S23            S33" << std::endl;
    }
    for (long i=0; i<M->NodeNumber; i++)
    {
        double TempU1, TempU2, TempU3;
        double TempS1, TempS2, TempS3, TempS4, TempS5, TempS6;
        double TempE1, TempE2, TempE3, TempE4, TempE5, TempE6;
        long RowU1=Dim*i;long RowU2=Dim*i+1;
        long RowU3;
        long RowS1=i*3*(Dim-1)+0;long RowS2=i*3*(Dim-1)+1;long RowS3=i*3*(Dim-1)+2;
        long RowS4, RowS5, RowS6;
        TempU1=L->U(RowU1);TempU2=L->U(RowU2);
        TempS1=L->S_Ord(RowS1);TempS2=L->S_Ord(RowS2);TempS3=L->S_Ord(RowS3);
        if (Dim==3)
        {
            RowU3=Dim*i+2;
            RowS4=3*(Dim-1)*i+3;
            RowS5=3*(Dim-1)*i+4;
            RowS6=3*(Dim-1)*i+5;
            TempS4=L->S_Ord(RowS4);TempS5=L->S_Ord(RowS5);TempS6=L->S_Ord(RowS6);
            TempU3=L->U(RowU3);
        }
        
        if (Dim==2)
        {
            string LastStr;
            LastStr=M->ToString(TempS3,5,15,true);
            Line= M->ToString(M->Nodes[i+1].NodeNum,0,15,false) + M->ToString(M->Nodes[i+1].X[0],5,15,false) + M->ToString(M->Nodes[i+1].X[1],5,15,false)
                 + M->ToString(TempU1,5,15,false)+ M->ToString(TempU2,5,15,false)
                 //+ ToString(abs(ErrorEstimU(i,0)),5,false)+ ToString(abs(ErrorEstimU(i,1)),5,false) 
                 + M->ToString(TempS1,5,15,false)+ M->ToString(TempS2,5,15,false)+ LastStr;
        }
        if (Dim==3)
        {
            Line= M->ToString(M->Nodes[i+1].NodeNum,0,15,false) + M->ToString(M->Nodes[i+1].X[0],5,15,false) + M->ToString(M->Nodes[i+1].X[1],5,15,false) + M->ToString(M->Nodes[i+1].X[2],5,15,false)
                 + M->ToString(TempU1,5,15,false)+ M->ToString(TempU2,5,15,false) + M->ToString(TempU3,5,15,false) 
                 + M->ToString(TempS1,5,15,false)+ M->ToString(TempS2,5,15,false)+ M->ToString(TempS3,5,15,false)
                 + M->ToString(TempS4,5,15,false)+ M->ToString(TempS5,5,15,false)+ M->ToString(TempS6,5,15,true);
        }
        *outfile << Line << std::endl;
    }
}

void OutputFile::PrintEstimExactSolution(Model* M, LinearProblem* L, std::ofstream* outfile)
{
    string Line;
    int Dim=M->Dimension;
    
    if (M->ExactSolutionAvailable==true)
    {
        *outfile << "#EXACT_SOLUTION" << endl;
        if (Dim==2)
        {
            *outfile << " NodeNum   U1             U2             S11            S12            S22" << std::endl;
        }
        else if (Dim==3)
        {
            *outfile << " NodeNum   U1             U2             U3             S11            S12            S13            S22            S23            S33" << std::endl;
        }
        for (int i=0; i<M->NodeNumber; i++)
        {
            string LineU, LineS;
            LineU="";
            for (int j=0; j<Dim; j++)
            {
                if (M->Nodes[i+1].ExactU!=NULL)
                {
                    LineU= LineU+M->ToString(M->Nodes[i+1].ExactU[j],5,15,false);
                }
                else
                {
                    LineU= LineU +"           ~,  ";
                }
            }
            LineS="";
            for (int j=0; j<3*(Dim-1)-1; j++)
            {
                if (M->Nodes[i+1].ExactS!=NULL)
                {
                    LineS= LineS+M->ToString(M->Nodes[i+1].ExactS[j],5,15,false);
                }
                else
                {
                    LineS= LineS+ "           ~,  ";
                }
            }
            //Last stress of the row
            if (M->Nodes[i+1].ExactS!=NULL)
            {
                LineS= LineS+M->ToString(M->Nodes[i+1].ExactS[3*(Dim-1)-1],5,15,true);
            }
            else
            {
                LineS= LineS+ "             ~";
            }
            Line= M->ToString(M->Nodes[i+1].NodeNum,0,15,false) + LineU + LineS;
            *outfile << Line << std::endl;
        }
    }
}

void OutputFile::CalculateErrorNorms(Model* M, LinearProblem* L, std::ofstream* outfile)
{
    bool ExactUAvailable=false, ExactSAvailable=false;
    if (M->Nodes[1].ExactS != NULL){ExactSAvailable=true;}
    if (M->Nodes[1].ExactU != NULL){ExactUAvailable=true;}
    
    if (ExactSAvailable==true || ExactUAvailable==true)
    {
        *outfile << "#ERROR_NORMS" << endl;
    }
    
    //Calculate the error norms
    if (ExactSAvailable==true)
    {
        M->ExactS.resize(M->NodeNumber*(3*(M->Dimension-1)));
        for (long i=0; i<M->NodeNumber; i++)
        {
            for (int j=0; j<3*(M->Dimension-1); j++)
            {
                M->ExactS[i*3*(M->Dimension-1)+j]=M->Nodes[i+1].ExactS[j];
            }
        }
        CalculatePrintExactErrorS(M,L,outfile);
    }
    if (ExactUAvailable==true)
    {
        M->ExactU.resize(M->NodeNumber*M->Dimension);
        for (int i=0; i<M->NodeNumber; i++)
        {
            for (int j=0; j<M->Dimension; j++)
            {
                M->ExactU[i*M->Dimension+j]=M->Nodes[i+1].ExactU[j];
            }
        }
        CalculatePrintExactErrorU(M,L,outfile);
    }
}

void OutputFile::CalculatePrintExactErrorS(Model* M, LinearProblem* L, std::ofstream* outfile)
{
    int Dim=M->Dimension;
    VectorXd ExactEpsilon;
    VectorXd NodeNum;
    NodeNum.resize(3*(Dim-1)+1);

    string Line1="", Line2="", Line3="", Line4="", Line5="", Line6="";
    bool LastItem=false;
    
    for (int j=0; j<3*(Dim-1)+1; j++)
    {
        if (j==3*(Dim-1)) {LastItem=true;}
        Line1=Line1 + M->ToString(M->ErrorNorm("L1_ERROR","",M->ExactS,L->S_Ord,j,3*(Dim-1)),5,15,LastItem);
        Line2=Line2 + M->ToString(M->ErrorNorm("L2_ERROR","",M->ExactS,L->S_Ord,j,3*(Dim-1)),5,15,LastItem);
        Line3=Line3 + M->ToString(M->ErrorNorm("L1_ERROR","WEIGHTED",M->ExactS,L->S_Ord,j,3*(Dim-1)),5,15,LastItem);
        Line4=Line4 + M->ToString(M->ErrorNorm("L2_ERROR","WEIGHTED",M->ExactS,L->S_Ord,j,3*(Dim-1)),5,15,LastItem);
        Line5=Line5 + M->ToString(M->ErrorNorm("L2_ERROR","RELATIVE",M->ExactS,L->S_Ord,j,3*(Dim-1)),5,15,LastItem);
        Line6=Line6 + M->ToString(M->ErrorNorm("LInf_ERROR","",M->ExactS,L->S_Ord,j,3*(Dim-1)),5,15,LastItem);
    }
    if (Dim==2)
    {
        *outfile << " ERROR_TYPE               S11            S12            S22            VMS" << endl;
    }
    else if (Dim==3)
    {
        *outfile << " ERROR_TYPE               S11            S12            S13            S22            S23            S33            VMS" << endl;
    }

    *outfile << " L1_ERROR                " << Line1 << endl;
    *outfile << " L2_ERROR                " << Line2 << endl;
    *outfile << " L1_ERROR_WEIGHTED       " << Line3 << endl;
    *outfile << " L2_ERROR_WEIGHTED       " << Line4 << endl;
    *outfile << " L2_ERROR_RELATIVE       " << Line5 << endl;
    *outfile << " LInf_ERROR              " << Line6 << endl;
    *outfile << "" << endl;
}

void OutputFile::CalculatePrintExactErrorU(Model* M, LinearProblem* L, std::ofstream* outfile)
{
    int Dim=M->Dimension;
    string Line1="", Line2="", Line3="";
    bool LastItem=false;
    for (int j=0; j<Dim; j++)
    {
        if (j==Dim-1) {LastItem=true;}
        Line1=Line1 + M->ToString(M->ErrorNorm("L2_ERROR","WEIGHTED",M->ExactU,L->U,j,Dim),5,15,LastItem);
        Line2=Line2 + M->ToString(M->ErrorNorm("L2_ERROR","RELATIVE",M->ExactU,L->U,j,Dim),5,15,LastItem);
        Line3=Line3 + M->ToString(M->ErrorNorm("LInf_ERROR","",M->ExactU,L->U,j,Dim),5,15,LastItem);
    }
    if (Dim==2)
    {
        *outfile << " ERROR_TYPE               U1             U2" << endl;
    }
    *outfile << " L2_ERROR_WEIGHTED       " << Line1 << endl;
    *outfile << " L2_ERROR_RELATIVE       " << Line2 << endl;
    *outfile << " LInf_ERROR              " << Line3 << endl;
    *outfile << "" << endl;
}

float OutputFile::GetAverageBCNodeSupport(Model* M)
{
    float AverageSupSize=0;
    long BCNodeNum=0;
    for (long i=1; i<=M->NodeNumber; i++)
    {
        if (M->Nodes[i].BoundaryNode == true)
        {
            BCNodeNum++;
            AverageSupSize=AverageSupSize+M->Nodes[i].SupSize;
        }
    }
    return AverageSupSize/BCNodeNum;
}

float OutputFile::GetAverageInnerNodeSupport(Model* M)
{
    float AverageSupSize=0;
    long IntNodeNum=0;
    for (long i=1; i<=M->NodeNumber; i++)
    {
        if (M->Nodes[i].BoundaryNode == false)
        {
            IntNodeNum++;
            AverageSupSize=AverageSupSize+M->Nodes[i].SupSize;
        }
    }
    return AverageSupSize/IntNodeNum;
}