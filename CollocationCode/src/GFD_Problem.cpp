#include "GFD_Problem.h"

GFD_Problem::GFD_Problem() {
}

GFD_Problem::GFD_Problem(const GFD_Problem& orig) {
}

GFD_Problem::~GFD_Problem() {
}

void GFD_Problem::InitializeProblem(Model* M, long* TripStressTreadStartIncTemp, long* TripStiffTreadStartIncTemp)
{
    int Dimension=M->Dimension;
    TripStressTreadStartInc=TripStressTreadStartIncTemp;
    TripStiffTreadStartInc=TripStiffTreadStartIncTemp;
    //Matrix Dim
    if (Dimension==2)
    {
        l=6;
    }
    else if (Dimension==3)
    {
        l=10;
    }
    Exponents.resize(l,Dimension);
    if (M->ModelType=="2D_PLANE_STRAIN" || Dimension==3)
    {
        d1=M->E*(1-M->nu)/((1+M->nu)*(1-2*M->nu));
        d2=M->E*M->nu/((1+M->nu)*(1-2*M->nu));
        d3=M->E*(1-2*M->nu)/((1+M->nu)*(1-2*M->nu));
    }
    else if (M->ModelType=="2D_PLANE_STRESS")
    {
        d1=M->E/(1-pow(M->nu,2));
        d2=M->E*M->nu/(1-pow(M->nu,2));
        d3=M->E*(1-M->nu)/(1-pow(M->nu,2));
    }
    
    //Initialise the Exponent matrix
    for (int i=0; i<l; i++)
    {
        for (int j=0; j<Dimension; j++)
        {
            Exponents(i,j)=0;
        }
    }
    if (Dimension==2)
    {
        //Initialise the Exponent matrix
        Exponents(1,0)=1;Exponents(2,1)=1;
        Exponents(3,0)=2;
        Exponents(4,0)=1;Exponents(4,1)=1;
        Exponents(5,1)=2;
    }
    else if (Dimension==3)
    {
        //Initialise the Exponent matrix
        Exponents(1,0)=1;Exponents(2,1)=1;Exponents(3,2)=1;
        Exponents(4,0)=2;Exponents(7,1)=2;Exponents(9,2)=2;
        Exponents(5,0)=1;Exponents(5,1)=1;
        Exponents(6,0)=1;Exponents(6,2)=1;
        Exponents(8,1)=1;Exponents(8,2)=1;
    }
}

void GFD_Problem::LoadDerivativeCoefficients(Model* M, int ThreadIndex, int TreadNum)
{
    long Start, End;
    //Get the start and end node of the thread
    M->GetStartEndNode(ThreadIndex,TreadNum,Start, End);
        
    //Add the coefficients to the defined functions
    for (long i=Start; i<=End; i++)
    {
        if (Start==1)
        {
            M->PrintLoading(i-Start,End-Start,"Node Derivative Loading: ", false);
        }
        int IndexOffset=1;
        
        //Get the initial matrix C1
        MatrixXd C1=C_Matrix(i,M);
        MatrixXd C2;
        MatrixXd C3;
                
        //Fill the matrix K for each particle of the support
        if (M->Nodes[i].CollocationPointOffset==true)
        {
            IndexOffset=0;
        }
        for (long j=0; j<M->Nodes[i].SupSize+1; j++)
        {
            for (int jj=0; jj<l-1; jj++)
            {
                M->Nodes[i].Coeff[j][jj]=C1(jj+1-IndexOffset,j);
            }
        }
    }
}

void GFD_Problem::LoadVectors(Model* M,double* F_Vec,T* TripStiff,T* TripStress,int ThreadIndex, int TreadNum)
{
    long Start, End;
    int Dimension=M->Dimension;
    
    //Get the start and end node of the thread
    M->GetStartEndNode(ThreadIndex,TreadNum,Start, End);
    
    long StressMatInc=TripStressTreadStartInc[ThreadIndex];
    long StiffMatInc=TripStiffTreadStartInc[ThreadIndex];
    
    for (long i=Start; i<=End; i++)
    {
        int SupSize;
        long LocNodeNum;
        
        //Fill the matrix K for each particle of the suppot
        SupSize=M->Nodes[i].SupSize;
        int IndexOffset=1;
        if (M->Nodes[i].CollocationPointOffset==true || M->SingularEnrichment != "")
        {
            IndexOffset=0;
        }
        for (long j=0; j<SupSize; j++)
        {
            int SupOffset=0;
            if (M->Nodes[i].SupNodes[0]==i && j>0)
                SupOffset=1;
            
            VectorXd Coeff(1);
            Coeff.resize(l-IndexOffset);
            for (int jj=0; jj<l-IndexOffset; jj++)
            {
                Coeff(jj)=M->Nodes[i].Coeff[j+SupOffset][jj];
            }
            //Get the number of the support or reference node
            if (j==0)
            {
                LocNodeNum=i;
            }
            else
            {
                LocNodeNum=M->Nodes[i].SupNodes[j-1+SupOffset];
            }
            if (Dimension==2)
            {
                LoadRows2D_Stress(Coeff,Coeff,i,LocNodeNum,j,Dimension,SupSize,TripStress,StressMatInc);
                LoadRows2D_Stiff(M,Coeff,Coeff,i,LocNodeNum,j,Dimension,SupSize,F_Vec,TripStiff,StiffMatInc);
            }
            else if (Dimension==3)
            {
                LoadRows3D_Stress(Coeff,i,LocNodeNum,j,Dimension,SupSize,TripStress,StressMatInc);
                LoadRows3D_Stiff(M,Coeff,i,LocNodeNum,j,Dimension,SupSize,F_Vec,TripStiff,StiffMatInc);
            }
        }
        
        for (int k=0; k<Dimension; k++)
        {
            if (M->Nodes[i].BoundaryNode==false)
                StiffMatInc+=SupSize*Dimension;
            else if (M->Nodes[i].BC_D_Bool[k]==true)
            {
                if (M->Nodes[i].LocalBoundaryCondition[k]==true)
                    StiffMatInc+=Dimension;
                else
                    StiffMatInc++;
            }
            else if (M->Nodes[i].BC_N_Bool[k]==true)
                StiffMatInc+=SupSize*Dimension;
        }
        StressMatInc+=SupSize*(6+9*(Dimension-2));
    }
}

void GFD_Problem::LoadRows2D_Stress(VectorXd Coeff,VectorXd Coeff2,long i,long LocNodeNum,int j,int Dimension, int SupSize,T* TripStress, long& StressMatInc)
{
    //Load the triplet vectors - Method GFD
    float TempVal1,TempVal2,TempVal3,TempVal4,TempVal5,TempVal6;
    //Stress Matrix - Equation 1 - Sigma11
    TempVal1=d1*Coeff(0);
    TempVal2=d2*Coeff2(1);
    TripStress[StressMatInc+SupSize*Dimension*0+j*Dimension+0]=T((i-1)*(Dimension+1)+0,(LocNodeNum-1)*Dimension+0,TempVal1);
    TripStress[StressMatInc+SupSize*Dimension*0+j*Dimension+1]=T((i-1)*(Dimension+1)+0,(LocNodeNum-1)*Dimension+1,TempVal2);
    //Stress Matrix - Equation 2 - Sigma12
    TempVal3=d3/2*Coeff(1);
    TempVal4=d3/2*Coeff2(0);
    TripStress[StressMatInc+SupSize*Dimension*1+j*Dimension+0]=T((i-1)*(Dimension+1)+1,(LocNodeNum-1)*Dimension+0,TempVal3);
    TripStress[StressMatInc+SupSize*Dimension*1+j*Dimension+1]=T((i-1)*(Dimension+1)+1,(LocNodeNum-1)*Dimension+1,TempVal4);
    //Stress Matrix - Equation 3 - Sigma22
    TempVal5=d2*Coeff(0);
    TempVal6=d1*Coeff2(1);
    TripStress[StressMatInc+SupSize*Dimension*2+j*Dimension+0]=T((i-1)*(Dimension+1)+2,(LocNodeNum-1)*Dimension+0,TempVal5);
    TripStress[StressMatInc+SupSize*Dimension*2+j*Dimension+1]=T((i-1)*(Dimension+1)+2,(LocNodeNum-1)*Dimension+1,TempVal6);
}

void GFD_Problem::LoadRows3D_Stress(VectorXd Coeff,long i,long LocNodeNum,int j,int Dimension, int SupSize,T* TripStress, long& StressMatInc)
{
    //Load the triplet vectors - Method GFD
    //Stress Matrix - Sigma11
    float TempVal11_1,TempVal11_2,TempVal11_3;
    TempVal11_1=d1*Coeff(0);
    TempVal11_2=d2*Coeff(1);
    TempVal11_3=d2*Coeff(2);
    TripStress[StressMatInc+SupSize*Dimension*0+j*Dimension+0]=T((i-1)*6+0,(LocNodeNum-1)*Dimension+0,TempVal11_1);
    TripStress[StressMatInc+SupSize*Dimension*0+j*Dimension+1]=T((i-1)*6+0,(LocNodeNum-1)*Dimension+1,TempVal11_2);
    TripStress[StressMatInc+SupSize*Dimension*0+j*Dimension+2]=T((i-1)*6+0,(LocNodeNum-1)*Dimension+2,TempVal11_3);
    //Stress Matrix - Sigma22
    float TempVal22_1,TempVal22_2,TempVal22_3;
    TempVal22_1=d2*Coeff(0);
    TempVal22_2=d1*Coeff(1);
    TempVal22_3=d2*Coeff(2);
    TripStress[StressMatInc+SupSize*Dimension*1+j*Dimension+0]=T((i-1)*6+1,(LocNodeNum-1)*Dimension+0,TempVal22_1);
    TripStress[StressMatInc+SupSize*Dimension*1+j*Dimension+1]=T((i-1)*6+1,(LocNodeNum-1)*Dimension+1,TempVal22_2);
    TripStress[StressMatInc+SupSize*Dimension*1+j*Dimension+2]=T((i-1)*6+1,(LocNodeNum-1)*Dimension+2,TempVal22_3);
    //Stress Matrix - Sigma33
    float TempVal33_1,TempVal33_2,TempVal33_3;
    TempVal33_1=d2*Coeff(0);
    TempVal33_2=d2*Coeff(1);
    TempVal33_3=d1*Coeff(2);
    TripStress[StressMatInc+SupSize*Dimension*2+j*Dimension+0]=T((i-1)*6+2,(LocNodeNum-1)*Dimension+0,TempVal33_1);
    TripStress[StressMatInc+SupSize*Dimension*2+j*Dimension+1]=T((i-1)*6+2,(LocNodeNum-1)*Dimension+1,TempVal33_2);
    TripStress[StressMatInc+SupSize*Dimension*2+j*Dimension+2]=T((i-1)*6+2,(LocNodeNum-1)*Dimension+2,TempVal33_3);
    //Stress Matrix - Sigma12
    float TempVal12_1,TempVal12_2;
    TempVal12_1=d3/2*Coeff(1);
    TempVal12_2=d3/2*Coeff(0);
    TripStress[StressMatInc+SupSize*Dimension*3+SupSize*2*0+j*2+0]=T((i-1)*6+3,(LocNodeNum-1)*Dimension+0,TempVal12_1);
    TripStress[StressMatInc+SupSize*Dimension*3+SupSize*2*0+j*2+1]=T((i-1)*6+3,(LocNodeNum-1)*Dimension+1,TempVal12_2);
    //Stress Matrix - Sigma13
    float TempVal13_1,TempVal13_3;
    TempVal13_1=d3/2*Coeff(2);
    TempVal13_3=d3/2*Coeff(0);
    TripStress[StressMatInc+SupSize*Dimension*3+SupSize*2*1+j*2+0]=T((i-1)*6+4,(LocNodeNum-1)*Dimension+0,TempVal13_1);
    TripStress[StressMatInc+SupSize*Dimension*3+SupSize*2*1+j*2+1]=T((i-1)*6+4,(LocNodeNum-1)*Dimension+2,TempVal13_3);
    //Stress Matrix - Sigma23
    float TempVal23_2,TempVal23_3;
    TempVal23_2=d3/2*Coeff(2);
    TempVal23_3=d3/2*Coeff(1);
    TripStress[StressMatInc+SupSize*Dimension*3+SupSize*2*2+j*2+0]=T((i-1)*6+5,(LocNodeNum-1)*Dimension+1,TempVal23_2);
    TripStress[StressMatInc+SupSize*Dimension*3+SupSize*2*2+j*2+1]=T((i-1)*6+5,(LocNodeNum-1)*Dimension+2,TempVal23_3);
}

void GFD_Problem::LoadRows2D_Stiff(Model* M,VectorXd Coeff,VectorXd Coeff2,long i,long LocNodeNum,int j,int Dimension, int SupSize, double* F_Vec,T* TripStiff, long& StiffMatInc)
{    
    float TempValU1_1,TempValU1_2,TempValU2_1,TempValU2_2;
    //Compute the equilibrium equation
    if (M->Nodes[i].BoundaryNode==false || M->Stabilization==true)
    {
        //Equilibrium - Equation 1
        TempValU1_1=d1*Coeff(2)+d3/2*Coeff(4);
        TempValU1_2=(d2+d3/2)*Coeff2(3);
        //Equilibrium - Equation 2
        TempValU2_1=(d2+d3/2)*Coeff(3);
        TempValU2_2=d1*Coeff2(4)+d3/2*Coeff2(2);
    }
    //If no boundary condition is applied to the node
    if (M->Nodes[i].BoundaryNode==false)
    {
        //Equilibrium - Equation 1
        TripStiff[StiffMatInc+SupSize*Dimension*0+j*Dimension+0]=T((i-1)*Dimension+0,(LocNodeNum-1)*Dimension+0,TempValU1_1);
        TripStiff[StiffMatInc+SupSize*Dimension*0+j*Dimension+1]=T((i-1)*Dimension+0,(LocNodeNum-1)*Dimension+1,TempValU1_2);
        //Equilibrium - Equation 2
        TripStiff[StiffMatInc+SupSize*Dimension*1+j*Dimension+0]=T((i-1)*Dimension+1,(LocNodeNum-1)*Dimension+0,TempValU2_1);
        TripStiff[StiffMatInc+SupSize*Dimension*1+j*Dimension+1]=T((i-1)*Dimension+1,(LocNodeNum-1)*Dimension+1,TempValU2_2);
        //Set the value of the force vector
        if (j==0)
        {
            F_Vec[(i-1)*Dimension+0]=0;
            F_Vec[(i-1)*Dimension+1]=0;
        }
    }
    else
    {
        //Load the triplet vectors - Method GFD
        float TempVal11_1,TempVal11_2,TempVal12_1,TempVal12_2,TempVal22_1,TempVal22_2;
        //Stress Matrix - Equation 1 - Sigma11
        TempVal11_1=d1*Coeff(0);
        TempVal11_2=d2*Coeff2(1);
        //Stress Matrix - Equation 2 - Sigma12
        TempVal12_1=d3/2*Coeff(1);
        TempVal12_2=d3/2*Coeff2(0);
        //Stress Matrix - Equation 3 - Sigma22
        TempVal22_1=d2*Coeff(0);
        TempVal22_2=d1*Coeff2(1);
        int IncBound=0;
        for (int k=0; k<Dimension; k++)
        {
            //Set the Dirichlet BC if any
            if (M->Nodes[i].BC_D_Bool[k]==true && j==0)
            {
                //For local boundary conditions add the normal terms to the boundary conditions
                if (M->Nodes[i].LocalBoundaryCondition[k]==true)
                {
                    //The boundary is in the direction of the normal
                    if (M->Nodes[i].BC_DOF[k]==0)
                    {
                        TripStiff[StiffMatInc+IncBound]=T((i-1)*Dimension+k,(i-1)*Dimension+0,-M->Nodes[i].Normal[k][0]);IncBound++;
                        TripStiff[StiffMatInc+IncBound]=T((i-1)*Dimension+k,(i-1)*Dimension+1,-M->Nodes[i].Normal[k][1]);IncBound++;
                    }
                    //The boundary is orthogonal to the normal direction
                    else if (M->Nodes[i].BC_DOF[k]==1)
                    {
                        TripStiff[StiffMatInc+IncBound]=T((i-1)*Dimension+k,(i-1)*Dimension+0,+M->Nodes[i].Normal[k][1]);IncBound++;
                        TripStiff[StiffMatInc+IncBound]=T((i-1)*Dimension+k,(i-1)*Dimension+1,-M->Nodes[i].Normal[k][0]);IncBound++;
                    }
                }
                else
                {
                    TripStiff[StiffMatInc+IncBound]=T((i-1)*Dimension+k,(i-1)*Dimension+k,1);IncBound++;
                }
                if (M->SingularEnrichment != "" && M->Nodes[i].EnrichedNode==true)
                    F_Vec[(i-1)*Dimension+k]=(M->Nodes[i].BC_D[k])/M->Nodes[i].U1[k];
                else
                    F_Vec[(i-1)*Dimension+k]=M->Nodes[i].BC_D[k];
            }
            else if (M->Nodes[i].BC_D_Bool[k]==true && j>0)
            {
                if (M->Nodes[i].LocalBoundaryCondition[k]==true)
                    IncBound+=Dimension;
                else
                    IncBound++;
            }
            //Set the Neumann BC if no Dirichlet BC is present for the considered DOF
            else if (M->Nodes[i].BC_N_Bool[k]==true && M->Nodes[i].BC_D_Bool[k]==false)
            {
                double theta=0;
                if (M->Nodes[i].LocalBoundaryCondition[k]==true)
                {
                    theta=Angle(M,i,k,-1);
                }
                if (M->Nodes[i].BC_DOF[k]==0)
                {
                    float TempVal7,TempVal8;
                    if (M->Nodes[i].LocalBoundaryCondition[k]==true)
                    {
                        double TempVal11_1_loc=TempVal11_1*pow(cos(theta),2)+TempVal12_1*sin(2*theta)+TempVal22_1*pow(sin(theta),2);
                        double TempVal11_2_loc=TempVal11_2*pow(cos(theta),2)+TempVal12_2*sin(2*theta)+TempVal22_2*pow(sin(theta),2);
                        double TempVal12_1_loc=-0.5*TempVal11_1*sin(2*theta)+TempVal12_1*cos(2*theta)+0.5*TempVal22_1*sin(2*theta);
                        double TempVal12_2_loc=-0.5*TempVal11_2*sin(2*theta)+TempVal12_2*cos(2*theta)+0.5*TempVal22_2*sin(2*theta);
                        TempVal7=TempVal11_1_loc*1 + TempVal12_1_loc*0;
                        TempVal8=TempVal11_2_loc*1 + TempVal12_2_loc*0;
                    }
                    else
                    {
                        TempVal7=TempVal11_1*M->Nodes[i].Normal[k][0] + TempVal12_1*M->Nodes[i].Normal[k][1];
                        TempVal8=TempVal11_2*M->Nodes[i].Normal[k][0] + TempVal12_2*M->Nodes[i].Normal[k][1];
                    }
                    if ((M->PartialStab==false && M->Stabilization==true) || (M->PartialStab==true && M->Nodes[i].BC_N[0] + M->Nodes[i].BC_N[1] == 0))
                    {
                        TempVal7=TempVal7-0.5*M->Nodes[i].CharactLength*M->Nodes[i].ProdN*TempValU1_1;
                        TempVal8=TempVal8-0.5*M->Nodes[i].CharactLength*M->Nodes[i].ProdN*TempValU1_2;
                    }
                    TripStiff[StiffMatInc+IncBound+j*Dimension+0]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+0,TempVal7);
                    TripStiff[StiffMatInc+IncBound+j*Dimension+1]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+1,TempVal8);
                    IncBound+=SupSize*Dimension;
                }
                else if (M->Nodes[i].BC_DOF[k]==1)
                {
                    float TempVal7,TempVal8;
                    if (M->Nodes[i].LocalBoundaryCondition[k]==true)
                    {
                        double TempVal12_1_loc=-0.5*TempVal11_1*sin(2*theta)+TempVal12_1*cos(2*theta)+0.5*TempVal22_1*sin(2*theta);
                        double TempVal12_2_loc=-0.5*TempVal11_2*sin(2*theta)+TempVal12_2*cos(2*theta)+0.5*TempVal22_2*sin(2*theta);
                        double TempVal22_1_loc=TempVal11_1*pow(sin(theta),2)-TempVal12_1*sin(2*theta)+TempVal22_1*pow(cos(theta),2);
                        double TempVal22_2_loc=TempVal11_2*pow(sin(theta),2)-TempVal12_2*sin(2*theta)+TempVal22_2*pow(cos(theta),2);
                        TempVal7=TempVal12_1_loc*1 + TempVal22_1_loc*0;
                        TempVal8=TempVal12_2_loc*1 + TempVal22_2_loc*0;
                    }
                    else
                    {
                        TempVal7=TempVal12_1*M->Nodes[i].Normal[k][0] + TempVal22_1*M->Nodes[i].Normal[k][1];
                        TempVal8=TempVal12_2*M->Nodes[i].Normal[k][0] + TempVal22_2*M->Nodes[i].Normal[k][1];
                    }
                    if ((M->PartialStab==false && M->Stabilization==true) || (M->PartialStab==true && M->Nodes[i].BC_N[0] + M->Nodes[i].BC_N[1] == 0))
                    {
                        TempVal7=TempVal7-0.5*M->Nodes[i].CharactLength*M->Nodes[i].ProdN*TempValU2_1;
                        TempVal8=TempVal8-0.5*M->Nodes[i].CharactLength*M->Nodes[i].ProdN*TempValU2_2;
                    }
                    TripStiff[StiffMatInc+IncBound+j*Dimension+0]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+0,TempVal7);
                    TripStiff[StiffMatInc+IncBound+j*Dimension+1]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+1,TempVal8);
                    IncBound+=SupSize*Dimension;
                }
                if (j==0)
                {
                    F_Vec[(i-1)*Dimension+k]=-M->Nodes[i].BC_N[k];
                }
            }
        }
    }
}

void GFD_Problem::LoadRows3D_Stiff(Model* M,VectorXd Coeff,long i,long LocNodeNum,int j,int Dimension, int SupSize, double* F_Vec,T* TripStiff, long& StiffMatInc)
{
    //If no boundary condition is applied to the node
    if (M->Nodes[i].BoundaryNode==false)
    {
        float TempVal1,TempVal2,TempVal3;
        float TempVal4,TempVal5,TempVal6;
        float TempVal7,TempVal8,TempVal9;
    
        //Equilibrium - Equation 1
        TempVal1=d1*Coeff(3)+d3/2*(Coeff(6)+Coeff(8));
        TempVal2=(d2+d3/2)*Coeff(4);
        TempVal3=(d2+d3/2)*Coeff(5);
        TripStiff[StiffMatInc+SupSize*Dimension*0+j*Dimension+0]=T((i-1)*Dimension+0,(LocNodeNum-1)*Dimension+0,TempVal1);
        TripStiff[StiffMatInc+SupSize*Dimension*0+j*Dimension+1]=T((i-1)*Dimension+0,(LocNodeNum-1)*Dimension+1,TempVal2);
        TripStiff[StiffMatInc+SupSize*Dimension*0+j*Dimension+2]=T((i-1)*Dimension+0,(LocNodeNum-1)*Dimension+2,TempVal3);
        //Equilibrium - Equation 2
        TempVal4=(d2+d3/2)*Coeff(4);
        TempVal5=d1*Coeff(6)+d3/2*(Coeff(3)+Coeff(8));
        TempVal6=(d2+d3/2)*Coeff(7);
        TripStiff[StiffMatInc+SupSize*Dimension*1+j*Dimension+0]=T((i-1)*Dimension+1,(LocNodeNum-1)*Dimension+0,TempVal4);
        TripStiff[StiffMatInc+SupSize*Dimension*1+j*Dimension+1]=T((i-1)*Dimension+1,(LocNodeNum-1)*Dimension+1,TempVal5);
        TripStiff[StiffMatInc+SupSize*Dimension*1+j*Dimension+2]=T((i-1)*Dimension+1,(LocNodeNum-1)*Dimension+2,TempVal6);
        //Equilibrium - Equation 3
        TempVal7=(d2+d3/2)*Coeff(5);
        TempVal8=(d2+d3/2)*Coeff(7);
        TempVal9=d1*Coeff(8)+d3/2*(Coeff(3)+Coeff(6));
        TripStiff[StiffMatInc+SupSize*Dimension*2+j*Dimension+0]=T((i-1)*Dimension+2,(LocNodeNum-1)*Dimension+0,TempVal7);
        TripStiff[StiffMatInc+SupSize*Dimension*2+j*Dimension+1]=T((i-1)*Dimension+2,(LocNodeNum-1)*Dimension+1,TempVal8);
        TripStiff[StiffMatInc+SupSize*Dimension*2+j*Dimension+2]=T((i-1)*Dimension+2,(LocNodeNum-1)*Dimension+2,TempVal9);
        //Set the value of the force vector
        if (j==0)
        {
            F_Vec[(i-1)*Dimension+0]=0;
            F_Vec[(i-1)*Dimension+1]=0;
            F_Vec[(i-1)*Dimension+2]=0;
        }
    }
    else
    {
        //Load the triplet vectors - Method GFD
        //Stress Matrix - Sigma11
        float TempVal11_1,TempVal11_2,TempVal11_3;
        TempVal11_1=d1*Coeff(0);
        TempVal11_2=d2*Coeff(1);
        TempVal11_3=d2*Coeff(2);
        //Stress Matrix - Sigma22
        float TempVal22_1,TempVal22_2,TempVal22_3;
        TempVal22_1=d2*Coeff(0);
        TempVal22_2=d1*Coeff(1);
        TempVal22_3=d2*Coeff(2);
        //Stress Matrix - Sigma33
        float TempVal33_1,TempVal33_2,TempVal33_3;
        TempVal33_1=d2*Coeff(0);
        TempVal33_2=d2*Coeff(1);
        TempVal33_3=d1*Coeff(2);
        //Stress Matrix - Sigma12
        float TempVal12_1,TempVal12_2;
        float TempVal12_3=0;
        TempVal12_1=d3/2*Coeff(1);
        TempVal12_2=d3/2*Coeff(0);
        //Stress Matrix - Sigma13
        float TempVal13_1,TempVal13_3;
        float TempVal13_2=0;
        TempVal13_1=d3/2*Coeff(2);
        TempVal13_3=d3/2*Coeff(0);
        //Stress Matrix - Sigma23
        float TempVal23_2,TempVal23_3;
        float TempVal23_1=0;
        TempVal23_2=d3/2*Coeff(2);
        TempVal23_3=d3/2*Coeff(1);
        
        float* Normal=NULL;
        Normal=new float[Dimension];
        float TempLocalValue=0;
                
        int IncBound=0;
        for (int k=0; k<Dimension; k++)
        {
            //Set the Dirichlet BC if any
            if (M->Nodes[i].BC_D_Bool[k]==true && j==0)
            {
                //Get the local normal assotiated with the boundary direction
                Get3DLocalNormals(M->Nodes[i].Normal[k],M->Nodes[i].BC_DOF[k],Normal);
                //For local boundary conditions add the normal terms to the boundary conditions
                if (M->Nodes[i].LocalBoundaryCondition[k]==true)
                {
                    //The boundary is in the direction of the normal
                    TripStiff[StiffMatInc+IncBound]=T((i-1)*Dimension+k,(i-1)*Dimension+0, M->Nodes[i].Normal[k][0]);IncBound++;
                    TripStiff[StiffMatInc+IncBound]=T((i-1)*Dimension+k,(i-1)*Dimension+1, M->Nodes[i].Normal[k][1]);IncBound++;
                    TripStiff[StiffMatInc+IncBound]=T((i-1)*Dimension+k,(i-1)*Dimension+2, M->Nodes[i].Normal[k][2]);IncBound++;
                }
                else
                {
                    TripStiff[StiffMatInc+IncBound]=T((i-1)*Dimension+k,(i-1)*Dimension+k,1);IncBound++;
                }
                F_Vec[(i-1)*Dimension+k]=M->Nodes[i].BC_D[k];
            }
            else if (M->Nodes[i].BC_D_Bool[k]==true && j>0)
            {
                if (M->Nodes[i].LocalBoundaryCondition[k]==true)
                    IncBound+=Dimension;
                else
                    IncBound++;
            }
            //Set the Neumann BC if no Dirichlet BC is present for the considered DOF
            else if (M->Nodes[i].BC_N_Bool[k]==true && M->Nodes[i].BC_D_Bool[k]==false)
            {
                float TempVal1,TempVal2,TempVal3;
                //Get the TempVal for the local boundary conditions
                if (M->Nodes[i].LocalBoundaryCondition[k]==true)
                {
                    Get3DRotatedTerms(M->Nodes[i].Normal[k],M->Nodes[i].BC_DOF[k],TempLocalValue,TempVal11_1,TempVal12_1,TempVal13_1,TempVal22_1,TempVal23_1,TempVal33_1,false);
                    TempVal1=TempLocalValue;
                    Get3DRotatedTerms(M->Nodes[i].Normal[k],M->Nodes[i].BC_DOF[k],TempLocalValue,TempVal11_2,TempVal12_2,TempVal13_2,TempVal22_2,TempVal23_2,TempVal33_2,false);
                    TempVal2=TempLocalValue;
                    Get3DRotatedTerms(M->Nodes[i].Normal[k],M->Nodes[i].BC_DOF[k],TempLocalValue,TempVal11_3,TempVal12_3,TempVal13_3,TempVal22_3,TempVal23_3,TempVal33_3,false);
                    TempVal3=TempLocalValue;
//                    bool NewVal=false;
//                    //Avoid zeros on the diagonal
//                    if ((k==0 && TempVal1==0 && j==0) || (k==1 && TempVal2==0 && j==0) || (k==2 && TempVal3==0 && j==0))
//                    {
//                        NewVal=true;
//                        M->Nodes[i].BC_DOF[k]++;
//                        if ( M->Nodes[i].BC_DOF[k]>M->Dimension)
//                            M->Nodes[i].BC_DOF[k]=1;
//                    }
//                    //Test if the direction is compatible with the dirichlef BCs
//                    Get3DLocalNormals(M->Nodes[i].Normal[k],M->Nodes[i].BC_DOF[k],Normal);
//                    int DirMax=0;
//                    float MaxDirVal=abs(Normal[0]);
//                    for (int ii=1; ii<M->Dimension; ii++)
//                    {
//                        if (abs(Normal[ii])>MaxDirVal)
//                        {
//                            MaxDirVal=abs(Normal[ii]);
//                            DirMax=ii;
//                        }
//                    }
//                    if (M->Nodes[i].BC_D_Bool[DirMax]==true)
//                    {
//                        NewVal=true;
//                        M->Nodes[i].BC_DOF[k]++;
//                        if ( M->Nodes[i].BC_DOF[k]>M->Dimension)
//                            M->Nodes[i].BC_DOF[k]=1;
//                    }
//                    if (NewVal==true)
//                    {
//                        Get3DRotatedTerms(M->Nodes[i].Normal[k],M->Nodes[i].BC_DOF[k],TempLocalValue,TempVal11_1,TempVal12_1,TempVal13_1,TempVal22_1,TempVal23_1,TempVal33_1,FlagVal);
//                        TempVal1=TempLocalValue;
//                        Get3DRotatedTerms(M->Nodes[i].Normal[k],M->Nodes[i].BC_DOF[k],TempLocalValue,TempVal11_2,TempVal12_2,TempVal13_2,TempVal22_2,TempVal23_2,TempVal33_2,false);
//                        TempVal2=TempLocalValue;
//                        Get3DRotatedTerms(M->Nodes[i].Normal[k],M->Nodes[i].BC_DOF[k],TempLocalValue,TempVal11_3,TempVal12_3,TempVal13_3,TempVal22_3,TempVal23_3,TempVal33_3,false);
//                        TempVal3=TempLocalValue;
//                    }
                }
                if (M->Nodes[i].BC_DOF[k]==0)
                {
                    //Neumann BC DOF1
                    if (M->Nodes[i].LocalBoundaryCondition[k]==false)
                    {
                        TempVal1=TempVal11_1*M->Nodes[i].Normal[k][0] + TempVal12_1*M->Nodes[i].Normal[k][1] + TempVal13_1*M->Nodes[i].Normal[k][2];
                        TempVal2=TempVal11_2*M->Nodes[i].Normal[k][0] + TempVal12_2*M->Nodes[i].Normal[k][1];
                        TempVal3=TempVal11_3*M->Nodes[i].Normal[k][0] +                                        TempVal13_3*M->Nodes[i].Normal[k][2];
                    }
                    TripStiff[StiffMatInc+IncBound+j*Dimension+0]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+0,TempVal1);
                    TripStiff[StiffMatInc+IncBound+j*Dimension+1]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+1,TempVal2);
                    TripStiff[StiffMatInc+IncBound+j*Dimension+2]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+2,TempVal3);
                    IncBound+=SupSize*Dimension;
                }
                else if (M->Nodes[i].BC_DOF[k]==1)
                {
                    //Neumann BC DOF2
                    if (M->Nodes[i].LocalBoundaryCondition[k]==false)
                    {
                        TempVal1=TempVal12_1*M->Nodes[i].Normal[k][0] + TempVal22_1*M->Nodes[i].Normal[k][1];
                        TempVal2=TempVal12_2*M->Nodes[i].Normal[k][0] + TempVal22_2*M->Nodes[i].Normal[k][1] + TempVal23_2*M->Nodes[i].Normal[k][2];
                        TempVal3=                                     + TempVal22_3*M->Nodes[i].Normal[k][1] + TempVal23_3*M->Nodes[i].Normal[k][2];
                    }
                    TripStiff[StiffMatInc+IncBound+j*Dimension+0]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+0,TempVal1);
                    TripStiff[StiffMatInc+IncBound+j*Dimension+1]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+1,TempVal2);
                    TripStiff[StiffMatInc+IncBound+j*Dimension+2]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+2,TempVal3);
                    IncBound+=SupSize*Dimension;
                }
                else if (M->Nodes[i].BC_DOF[k]==2)
                {
                    //Neumann BC DOF3
                    if (M->Nodes[i].LocalBoundaryCondition[k]==false)
                    {
                        TempVal1=TempVal13_1*M->Nodes[i].Normal[k][0]                                        + TempVal33_1*M->Nodes[i].Normal[k][2];
                        TempVal2=                                       TempVal23_2*M->Nodes[i].Normal[k][1] + TempVal33_2*M->Nodes[i].Normal[k][2];
                        TempVal3=TempVal13_3*M->Nodes[i].Normal[k][0] + TempVal23_3*M->Nodes[i].Normal[k][1] + TempVal33_3*M->Nodes[i].Normal[k][2];
                    }
                    TripStiff[StiffMatInc+IncBound+j*Dimension+0]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+0,TempVal1);
                    TripStiff[StiffMatInc+IncBound+j*Dimension+1]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+1,TempVal2);
                    TripStiff[StiffMatInc+IncBound+j*Dimension+2]=T((i-1)*Dimension+k,(LocNodeNum-1)*Dimension+2,TempVal3);
                    IncBound+=SupSize*Dimension;
                }
                if (j==0)
                {
                    F_Vec[(i-1)*Dimension+k]=-M->Nodes[i].BC_N[k];
                }
            }
        }
        
        delete[] Normal;
    }
}

void GFD_Problem::Get3DLocalNormals(float* n1, int Dim, float* LocNormal)
{
    if (n1[1]==0 && n1[2]==0)
    {
        if (Dim==0)
        {
            LocNormal[0]=n1[0];
            LocNormal[1]=n1[1];
            LocNormal[2]=n1[2];
        }
        else if (Dim==1)
        {
            LocNormal[0]=0;
            LocNormal[1]=1;
            LocNormal[2]=0;
            if (n1[0]<0)
                LocNormal[1]=-1;
        }
        else if (Dim==2)
        {
            LocNormal[0]=0;
            LocNormal[1]=0;
            LocNormal[2]=1;
        }
    }
    else if (Dim==0)
    {
        LocNormal[0]=n1[0];
        LocNormal[1]=n1[1];
        LocNormal[2]=n1[2];
    }
    else if (Dim==1 || Dim==2)
    {
        //Gram Schmidt Process with U1 = N and U2= (1,0,0)
        double ScalarProd=pow(n1[0],2)+pow(n1[1],2)+pow(n1[2],2);
        double n20=1-1*n1[0]/ScalarProd*n1[0];
        double n21=-1*n1[0]/ScalarProd*n1[1];
        double n22=-1*n1[0]/ScalarProd*n1[2];
        if (Dim==1)
        {
            LocNormal[0]=n20;
            LocNormal[1]=n21;
            LocNormal[2]=n22;
        }
        else if (Dim==2)
        {
            LocNormal[0]=n1[1]*n22-n1[2]*n21;
            LocNormal[1]=n1[2]*n20-n1[0]*n22;
            LocNormal[2]=n1[0]*n21-n1[1]*n20;
        }
    }
    double Norm=0;
    for (int i=0; i<3; i++)
    {
        Norm+=pow(LocNormal[i],2);
    }
    Norm=pow(Norm,0.5);
    for (int i=0; i<3; i++)
    {
        LocNormal[i]=LocNormal[i]/Norm;
    }
}

void GFD_Problem::Get3DRotatedTerms(float* n1, int Dim, float& TempVal, float S11, float S12, float S13, float S22, float S23, float S33, bool Flag)
{
    float* LocalNormal=NULL;
    LocalNormal=new float[3];
    MatrixXd R(1,1);
    R.resize(3,3);
    MatrixXd S(1,1);
    S.resize(3,3);
    S(0,0)=S11;S(0,1)=S12;S(0,2)=S13;
    S(1,0)=S12;S(1,1)=S22;S(1,2)=S23;
    S(2,0)=S13;S(2,1)=S23;S(2,2)=S33;
    //Load the rotation matrix R
    for (int i=0; i<3; i++)
    {
        Get3DLocalNormals(n1,i,LocalNormal);
        for (int j=0; j<3; j++)
        {
            R(j,i)=LocalNormal[j];
        }
    }
    delete[] LocalNormal;
    MatrixXd S2=R.transpose()*S*R;
    TempVal=S2(Dim,0);
    if (Flag==true)
    {
        cout << "R=" << R << endl;
        cout << "S=" << S << endl;
        cout << "S2=" << S2 << endl;
    }
}

MatrixXd GFD_Problem::C_Matrix(int NodeIndex, Model* M)
{
    MatrixXd A(1,1);
    MatrixXd B(1,1);
    MatrixXd C;
    int SupSize=M->Nodes[NodeIndex].SupSize;
    int MatA_Size=l-1;
    int StartIndex=1;
    
    if (M->Nodes[NodeIndex].CollocationPointOffset==true)
    {
        MatA_Size=l;
        StartIndex=0;
    }
    
    A.resize(MatA_Size,MatA_Size);
    B.resize(MatA_Size,SupSize+StartIndex);
    
    //Initialize the matrices A and B;
    for (int i=0; i<MatA_Size; i++)
    {
        for (int j=0; j<MatA_Size; j++)
        {
            A(i,j)=0;
        }
        B(i,0)=0;
    }
    
    for (int i=0; i<SupSize; i++)
    {
        float z_i=0;
        long LocNodeNum=M->Nodes[NodeIndex].SupNodes[i];
        
        //Get the distance Z_i of the support node to the reference Node
        if (M->Nodes[NodeIndex].HasDiffractedSupNodes==false)
        {
            for (int m=0; m<M->Dimension; m++)
            {
                if (M->Nodes[NodeIndex].CollocationPointOffset==true)
                {
                    z_i+=pow(M->Nodes[LocNodeNum].X[m]-M->Nodes[NodeIndex].CollocX[m],2);
                }
                else
                {
                    z_i+=pow(M->Nodes[LocNodeNum].X[m]-M->Nodes[NodeIndex].X[m],2);
                }
            }
            z_i=pow(z_i,0.5);
        }
        else
        {
            z_i=M->Nodes[NodeIndex].DiffractedDist[i];
        }
        
        //Get Voronoi weight if the function is selected
        float SupNodeVol=1;
        if (M->Voronoi==true && M->Nodes[NodeIndex].BoundaryNode==false)
            SupNodeVol=M->Nodes[NodeIndex].SupNodeVol[i];
        
        //Calculate the square of the weight of the considered support node
        float W_sq=pow(SupNodeVol*w(M,z_i/pow(M->Nodes[NodeIndex].SupRadiusSq,0.5)),2);
               
        //Calculate the zero moments
        if (M->Nodes[NodeIndex].CollocationPointOffset==false)
        {
            for (int j=StartIndex; j<l; j++)
            {
                float Value=1;
                for (int m=0; m<M->Dimension; m++)
                {
                    Value= Value*pow(M->Nodes[LocNodeNum].X[m]-M->Nodes[NodeIndex].X[m],Exponents(j,m))/factorial(Exponents(j,m)); 
                }
                B(j-StartIndex,0)=B(j-StartIndex,0)-W_sq*Value;
            }
        }
        
        //Calculte the other moments
        for (int j=StartIndex; j<l; j++)
        {
            float Value1=1;
            for (int m=0; m<M->Dimension; m++)
            {
                if (M->Nodes[NodeIndex].CollocationPointOffset==true)
                {
                    Value1= Value1*pow(M->Nodes[LocNodeNum].X[m]-M->Nodes[NodeIndex].CollocX[m],Exponents(j,m))/factorial(Exponents(j,m));
                }
                else
                {
                    Value1= Value1*pow(M->Nodes[LocNodeNum].X[m]-M->Nodes[NodeIndex].X[m],Exponents(j,m))/factorial(Exponents(j,m));
                }
            }
            for (int jj=j; jj<l; jj++)
            {
                float Value2=1;
                for (int m=0; m<M->Dimension; m++)
                {
                    if (M->Nodes[NodeIndex].CollocationPointOffset==true)
                    {
                        Value2= Value2*pow(M->Nodes[LocNodeNum].X[m]-M->Nodes[NodeIndex].CollocX[m],Exponents(jj,m))/factorial(Exponents(jj,m)); 
                    }
                    else
                    {
                        Value2= Value2*pow(M->Nodes[LocNodeNum].X[m]-M->Nodes[NodeIndex].X[m],Exponents(jj,m))/factorial(Exponents(jj,m)); 
                    }
                }
                float Value=W_sq*Value1*Value2;
                A(j-StartIndex,jj-StartIndex)+=Value;
                
                if (j != jj)
                {
                    //Fill the symmetric part of the matrix A
                    A(jj-StartIndex,j-StartIndex)+=Value;
                }
                else
                {
                    //Fill the moment components
                    B(j-StartIndex,i+StartIndex)=W_sq*Value2;
                }
            }
        }
    }
    C=A.lu().solve(B);
    return C;
}

float GFD_Problem::w(Model* M,double s)
{
    float ww=0;
    float Expo=0;
    s=abs(s);
    float Limit=1;
    if(M->WindowFunctionType=="EXP1" || M->WindowFunctionType=="EXP2")
    {
        if(M->WindowFunctionType=="EXP1") 
            Expo=(float)1;
        else if(M->WindowFunctionType=="EXP2")
            Expo=(float)2.5;
        if (s<=(float)1)
        {
            ww=exp(-(pow(s,Expo)/pow(M->SupRatio,2)));
            ww=1-s;
        }
        else
        {
            ww=0;   
        }
    }
    else if(M->WindowFunctionType=="SPLINE_3")
    {
        if (s>=0 && s<0.5)
        {
            ww=(float)2/(float)3-4*pow(s,2)+4*pow(s,3);
        }
        else if (s>0.5 && s<1)
        {
            ww=(float)4/(float)3-4*s+4*pow(s,2)-(float)4/(float)3*pow(s,3);
        }
        else if (s>=1)
        {
            ww=0;
        }
        if (ww>0)
            ww=pow(ww,M->SupExp);
        else
            ww=0;
    }
    else if(M->WindowFunctionType=="SPLINE_4")
    {
        if (s<Limit)
        {
            ww=1-6*pow(s,2)+8*pow(s,3)-3*pow(s,4);
        }
        else if (s>=Limit)
        {
            ww=0;
        }
        if (ww>0)
            ww=pow(ww,M->SupExp);
        else
            ww=0;
    }
    else if(M->WindowFunctionType=="LINEAR")
    {
        if (s<1)
        {
            ww=1-s;
        }
        else if (s>=1)
        {
            ww=0;
        }
    }
    return ww;
}

float GFD_Problem::factorial(int a)
{
    float TempVal=1;
    for (int i=1; i<=a; i++)
    {
        TempVal=TempVal*i;
    }
    return TempVal;
}

double GFD_Problem::Angle(Model* M,long CollocNode, int Dim,long SingNodes)
{
    double Value=0;
    double ValX=0;
    double ValY=0;
    if (SingNodes==-1)
    {
        ValX=M->Nodes[CollocNode].Normal[Dim][0];
        ValY=M->Nodes[CollocNode].Normal[Dim][1];
    }
    else
    {
        ValX=M->Nodes[CollocNode].X[0]-M->Nodes[SingNodes].X[0];
        ValY=M->Nodes[CollocNode].X[1]-M->Nodes[SingNodes].X[1];
    }
    double Dist=pow(pow(ValX,2)+pow(ValY,2),0.5);
    if (ValY>=0)
        Value=acos(ValX/Dist);
    else
        Value=-acos(ValX/Dist);
    return Value;
}