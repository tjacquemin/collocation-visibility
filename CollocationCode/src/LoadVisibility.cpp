#include "Model.h"

void Model::LoadVisibilitySupport(long i,long* IndexArray,double* DistArray,int k, int SupSize)
{
    int SingularNodeInSupport=-1;
    //Initialize an array with information on the inclusion of the node in the support
    bool* SingularNode=NULL;
    SingularNode=new bool[k];
    
    bool* IncludeSupNode=NULL;
    IncludeSupNode=new bool[k];
    for(int j=0; j<k; j++)
    {
        SingularNode[j]=false;
        IncludeSupNode[j]=false;
        if (Nodes[IndexArray[j]].IsSingular==true)
        {
            SingularNodeInSupport=IndexArray[j];
            if (IncludeSingularNodes==false)
                SingularNode[j]=true;
        }
    }
    
    //Number of nodes in the support
    int CountSup=0;
    double SupRad=0;
    //Determine the nodes to be discarded from the support (singular nodes + visibility criteria)
    SelectSupportNodes(i,IndexArray,DistArray,IncludeSupNode,SingularNodeInSupport,SingularNode,SupSize,k,&CountSup,&SupRad);
    Nodes[i].SetSupRadius(pow(SupRad*SupScaling,2));        
    Nodes[i].SupSize=CountSup;
    Nodes[i].SupNodes = new long[CountSup];
    int j=0;
    int index=0;
    for (j=0; j<k; j++)
    {
        if (IncludeSupNode[j]==true)
        {
            Nodes[i].SupNodes[index]=IndexArray[j];
            index++;
        }
    }    
    delete[] SingularNode;
}

void Model::SelectSupportNodes(long i,long* IndexArray,double* DistArray,bool* IncludeSupNode,int SingularNodeInSupport,bool* SingularNode,int SupSize, int k, int* CountSup,double* SupRad)
{
    int Count=0;
//    if (SingularNodeInSupport==-1 || VisibilityType=="NONE")
    if (VisibilityType=="NONE")
    {
        *SupRad=DistArray[SupSize-1]*1.001;
        for (int j=0; j<k; j++)
        {
            if(DistArray[j]<*SupRad && SingularNode[j]==false)
            {
                IncludeSupNode[j]=true;
                Count++;
            }
        }
        *CountSup=Count;
    }
    else if (VisibilityType=="DIRECT")
    {
        int j=0;
        Nodes[i].HasSingularSupNodes=true;
        while (Count<SupSize)
        {
            if (SingularNode[j]==false && IsHidden(i,IndexArray[j],SingularNodeInSupport,DistArray[SupSize-1])==false)
            {
                IncludeSupNode[j]=true;
                Count++;
            }
            j++;
        }
        *SupRad=DistArray[j-1]*1.001;
        while (j<k)
        {
            if (DistArray[j]<*SupRad)
            {
                if (SingularNode[j]==false && IsHidden(i,IndexArray[j],SingularNodeInSupport,DistArray[SupSize-1])==false)
                {
                    IncludeSupNode[j]=true;
                    Count++;
                }
            }
            j++;
        }
        *CountSup=Count;
    }
    else if (VisibilityType=="DIFFRACTION")
    {
        //Create a vector pair to sort the distances
        vector<vector<double>> DistPair;
        DistPair.resize(k);
        for (int j=0; j<k; j++)
        {
            vector<double> TempVect;
            TempVect.resize(2);
            //Determine the updated distances based on the diffraction criterion
            if (IsHidden(i,IndexArray[j],SingularNodeInSupport,DistArray[SupSize-1])==true && SingularNode[j]==false)
            {
                double dist1=0,dist2=0;
                for (int kk=0; kk<Dimension; kk++)
                {
                    dist1+=pow(Nodes[i].X[kk]-Nodes[SingularNodeInSupport].X[kk],2);
                    dist2+=pow(Nodes[SingularNodeInSupport].X[kk]-Nodes[IndexArray[j]].X[kk],2);
                }
                DistArray[j]=pow(dist1,0.5)+pow(dist2,0.5);
            }
            TempVect[0]=DistArray[j];
            TempVect[1]=IndexArray[j];
            DistPair[j]=TempVect;
        }
        //Sort the updated distance array
        sort(DistPair.begin(),DistPair.end(), [](const vector< double >& a, const vector< double >& b){ return a[0] < b[0]; } );
        *SupRad=DistPair[SupSize-1][0]*1.001;
        for (int j=0; j<k; j++)
        {
            long IndexVal=DistPair[j][1];
            SingularNode[j]=Nodes[IndexVal].IsSingular;
            if(SingularNode[j]==false && DistPair[j][0]<*SupRad)
            {
                IncludeSupNode[Count]=true;
                DistArray[Count]=DistPair[j][0];
                IndexArray[Count]=DistPair[j][1];
                Count++;
            }
            else if (SingularNode[j]==true && DistPair[j][0]<*SupRad)
            {
                IncludeSupNode[Count]=false;
                *SupRad=DistPair[SupSize][0]*1.001;
            }
        }
        *CountSup=Count;
        
        //Indicate that the collocation node has diffracted nodes on its support
        Nodes[i].HasDiffractedSupNodes=true;
        Nodes[i].DiffractedDist=new double[Count];
        //Load the diffracted distance array
        for (int j=0; j<Count; j++)
        {
            Nodes[i].DiffractedDist[j]=DistArray[j];
        }
    }
}

void Model::LoadConnectedElements()
{
    for (long i=0; i<ElementNumber; i++)
    {
        SurfElement[i].LoadConnectionsToNodes();
        SurfElement[i].LoadCenterNode();
    }
}

void Model::LoadVisibilitySupport2D(long i,long* IndexArray,double* DistArray,int k, int SupSize)
{
    //Get all the elements in the support
    vector<long> ElementsInSup;
    ElementsInSup.clear();
    long SupNodeNum=0;
    for (int j=0; j<k; j++)
    {
        SupNodeNum=IndexArray[j];
        if (Nodes[SupNodeNum].AttachedElements.size()>0)
        {
            for (int k=0; k<Nodes[SupNodeNum].AttachedElements.size(); k++)
            {
                ElementsInSup.push_back(Nodes[SupNodeNum].AttachedElements[k]);
            }
        }
    }
    //Sort the elements vector and remove the duplicates
    if (ElementsInSup.size()>0)
    {
        sort(ElementsInSup.begin(),ElementsInSup.end());
        ElementsInSup.erase(unique(ElementsInSup.begin(),ElementsInSup.end()),ElementsInSup.end());
    }
    
    Tree LocTree;
    //Load the elements centroid nodes into a tree
    int ElSupSize=ElementsInSup.size();
    std::vector<Point_3> points(ElSupSize);
    std::vector<long> indices(ElSupSize);
    
    for (long ii=0; ii<ElSupSize; ii++)
    {
        if (Dimension==2)
            points[ii]=Point_3(SurfElement[ElementsInSup[ii]-1].CenterNode.X[0],SurfElement[ElementsInSup[ii]-1].CenterNode.X[1],0);
        else if (Dimension==3)
            points[ii]=Point_3(SurfElement[ElementsInSup[ii]-1].CenterNode.X[0],SurfElement[ElementsInSup[ii]-1].CenterNode.X[1],SurfElement[ElementsInSup[ii]-1].CenterNode.X[2]);
        indices[ii]=ElementsInSup[ii];
    }
    
    // Insert number_of_data_points in the tree
    LocTree.insert(
    boost::make_zip_iterator(boost::make_tuple(points.begin(),indices.begin())),
    boost::make_zip_iterator(boost::make_tuple(points.end(),indices.end())));
    
    //Identify the nodes which shall be included in the support
    bool* IncludeSupNode=NULL;
    IncludeSupNode=new bool[k];
    
    //For each node of the IndexArray, assess if the nodes shall be included in the support
    bool SupLoaded=false;
    int SupNodeIndex=0;
    int CountSupNodes=0;
    double SupRad=0;
    
    //Get the center of the search segment
    double* SupSegmentCenter=NULL;
    SupSegmentCenter=new double[Dimension];
    
    //Loop through all the support nodes until the support is loaded
    while (SupLoaded==false && SupNodeIndex<k)
    {
        IncludeSupNode[SupNodeIndex]=true;
        //If the supports includes any boundary elements
        if (ElementsInSup.size()>0)
        {
            //Get a reduced vector containing the elements the closest to the support segment considered
            vector<long> ElementsInSupLoc;
            ElementsInSupLoc.clear();
            //Get the center of the search segment
            for (int l=0; l<Dimension; l++)
            {
                SupSegmentCenter[l]=(Nodes[i].X[l]+Nodes[IndexArray[SupNodeIndex]].X[l])/2;
            }
            //Create the search query
            Point_3* query=NULL;
            if (Dimension==2)
                query= new Point_3(SupSegmentCenter[0], SupSegmentCenter[1], 0);
            else
                query= new Point_3(SupSegmentCenter[0], SupSegmentCenter[1], SupSegmentCenter[2]);
            
            //Number of selected elements
            int SupElNum=5*Dimension;
            if (ElementsInSup.size()<SupElNum)
                SupElNum=ElementsInSup.size();

            //Search the K nearest neighbours
            Distance tr_dist;
            K_neighbor_search search(LocTree, *query, SupElNum, 0.0,true,tr_dist,true);
            
            //Fill the index and distance array with the values using the iterator
            for(K_neighbor_search::iterator it = search.begin(); it != search.end(); it++)
            {
                ElementsInSupLoc.push_back(boost::get<1>(it->first));
            }
            delete query;
            
            //Loop over all boundary elments and assess if there is any intersection with the considered ray
            for (int j=0; j<ElementsInSupLoc.size(); j++)
            {
                bool Intersect = IntersectBoundary(i,IndexArray[SupNodeIndex],ElementsInSupLoc[j]);
                if (Intersect==true)
                {
                    IncludeSupNode[SupNodeIndex]=false;
                }
            }
        }
        if (IncludeSupNode[SupNodeIndex]==true)
        {
            if (SupRad==0){
                CountSupNodes++;}
            else if (DistArray[SupNodeIndex]<=SupRad){
                CountSupNodes++;}
            else if (DistArray[SupNodeIndex]>SupRad){
                SupLoaded=true;}
        }
        //Set the sup radius
        if (CountSupNodes==SupSize && SupRad==0)
        {
            SupRad=DistArray[SupNodeIndex]*1.001;
        }
        SupNodeIndex++;
    }
    delete[] SupSegmentCenter;
    
    //Nodes[i].SetSupRadius(pow(SupRad*SupScaling,2));
    Nodes[i].SupRadiusSq=pow(SupRad,2);
    Nodes[i].SupRadius=SupRad;
    Nodes[i].SupSize=CountSupNodes;
    Nodes[i].SupNodes = new long[CountSupNodes];
    int j=0;
    int index=0;
    while (index<CountSupNodes)
    {
        if (IncludeSupNode[j]==true)
        {
            Nodes[i].SupNodes[index]=IndexArray[j];
            index++;
        }
        //List the discarded nodes from the visibility criterion
        else if (IncludeSupNode[j]==false && DistArray[j]<SupRad)
        {
            cout << " i=" << i << ", SupNode=" << IndexArray[j] << endl;
        }
        j++;
    }
}

bool Model::IsHidden(long CollocationNodeNum, long SupportNodeNum, long SingularNodeNum, double Radius)
{
    bool TempResult=false;
    if (Dimension==2)
    {
        Vector3d SupVect;
        Vector3d SingVect;
        Vector3d CollocNode;
        Vector3d SingNode;
        SingularNodeNum=SingularNode;
        for (int i=0; i<Dimension; i++)
        {
            CollocNode(i)=Nodes[CollocationNodeNum].X[i];
            SingNode(i)=Nodes[SingularNodeNum].X[i];

            SupVect(i)=Nodes[SupportNodeNum].X[i]-Nodes[CollocationNodeNum].X[i];
            SingVect(i)=400*Nodes[SingularNodeNum].Normal[0][i];//Radius*
        }
        CollocNode(2)=0;
        SingNode(2)=0;
        SupVect(2)=0;
        SingVect(2)=0;

        float t, u;
        //Solution of the problem: Sing + t SingVect = Colloc + u SupVect  
        //                         t = (CollocNode − SingNode) × SupVect / (SingVect × SupVect)
        //                         u = (CollocNode − SingNode) × SingVect / (SingVect × SupVect)
        Vector3d P1, P2, P3, P4, P5, P6;
        P1=SingVect.cross(SupVect);
        P2=SupVect.cross(SingVect);
        P5=CollocNode - SingNode;
        P6=SingNode - CollocNode;
        P3=P5.cross(SupVect);
        P4=P6.cross(SingVect);
        
        t=P3(2)/P1(2);
        u=P4(2)/P2(2);
               
        //if (t>0 && t<1 && u>0 && u<1)
        if (t>=0 && t<=1 && u>=0 && u<=1)
        {
            TempResult=true;
        }
        if (CollocationNodeNum==SingularNodeNum)
        {
            TempResult=false;
        }
    }
    else if (Dimension==3)
    {
        //Test at 10 locations of the segment connecting the support node to the
        //collocation node that the point is in the L-Shape domain
        double X_Val=0;
        double Y_Val=0;
        double Z_Val=0;
        for (int i=1; i<10; i++)
        {
            X_Val=Nodes[CollocationNodeNum].X[0]+(Nodes[SupportNodeNum].X[0]-Nodes[CollocationNodeNum].X[0])/10*i;
            Y_Val=Nodes[CollocationNodeNum].X[1]+(Nodes[SupportNodeNum].X[1]-Nodes[CollocationNodeNum].X[1])/10*i;
            Z_Val=Nodes[CollocationNodeNum].X[2]+(Nodes[SupportNodeNum].X[2]-Nodes[CollocationNodeNum].X[2])/10*i;
            if (X_Val>0 && Y_Val>0 && Z_Val>0)
            {
                TempResult=true;
            }
        }
    }
    return TempResult;
}

bool Model::IntersectBoundary(long ColocNodeN, long SupNode, long ElNum)
{
    Node** StartEndNode=NULL;
    StartEndNode=new Node*[2];
    int Last_i_Index=1;
    if (ColocNodeN==0)
        Last_i_Index=2;
    
    double ThresholdValue=cos((90-VisibilityThresholdAngle)*pi/180);
    bool TempResult=false;
    double Epsilon2=sin(VisibilityThresholdAngle*pi/180);
    double Epsilon3=VisibilityThresholdAngle*pi/180;
        
    for (int ii=0; ii<Last_i_Index; ii++)
    {
        if (ColocNodeN>0)
        {
            StartEndNode[0]=SurfElement[ElNum-1].AttachedNodes[0];
            StartEndNode[1]=SurfElement[ElNum-1].AttachedNodes[1];
            if (Dimension==3)
                StartEndNode[2]=SurfElement[ElNum-1].AttachedNodes[2];
        }
        else
        {
            ThresholdValue=cos((90-0)*pi/180);
            Epsilon2=0;
            Epsilon3=0;
            if (ii==0)
            {
                StartEndNode[0]=SurfElement[ElNum-1].AttachedNodes[0];
                StartEndNode[1]=&SurfElement[ElNum-1].CenterNode;
                
            }
            else if (ii==1)
            {
                StartEndNode[0]=&SurfElement[ElNum-1].CenterNode;
                StartEndNode[1]=SurfElement[ElNum-1].AttachedNodes[1];
            }
        }

        //Create and load the matrices A and C
        MatrixXd A;
        A.resize(Dimension,Dimension);
        MatrixXd C;
        C.resize(Dimension,1);
        for (int i=0; i<Dimension; i++)
        {
            A(i,0)=Nodes[ColocNodeN].X[i]-Nodes[SupNode].X[i];
            for (int j=1; j<Dimension; j++)
            {
                 A(i,j)=StartEndNode[j]->X[i]-StartEndNode[0]->X[i];
            }
            C(i,0)=Nodes[ColocNodeN].X[i]-StartEndNode[0]->X[i];
        }

        //Get the angle between the element and the segment
        double SegmentElAngle=GetSegmentElementAngle(A);

        //Create the element and support vectors
        Vector3d SupVect;
        SupVect(2)=0;
        double DistVect=0;
        //Load the element and support vectors
        for (int i=0; i<Dimension; i++)
        {
            SupVect(i)=Nodes[SupNode].X[i]-Nodes[ColocNodeN].X[i];
            DistVect=pow(SupVect(i),2);
        }
        DistVect=pow(DistVect,0.5);
                
        if (abs(A.determinant())>0)
        {
            //Define a threshold for coincident points (1000th of the distance between the colloc node and the support node)
            double Epsilon1=DistVect/1000;
            float t, u;
            float v=0.5;

            if ((abs(SegmentElAngle))>ThresholdValue)
            {            
                //Solve the linear system
                MatrixXd B=A.lu().solve(C);
                t=B(0,0);
                u=B(1,0);
                if (Dimension==3)
                {
                    v=B(2,0);
                }
                //If the support ray intersects the element in its middle section
                if (t>Epsilon1 && t<1-Epsilon1 && u>Epsilon1 && u<1-Epsilon1 && v>Epsilon1 && v<1-Epsilon1)
                {
                    if (Dimension==3 && u+v<1-Epsilon1)
                    {
                        TempResult=true;
                    }
                    else if (Dimension==2)
                    {
                        TempResult=true;
                    }
                }
                else
                {
                    double ScalarProd=0;
                    long ElNumTemp=0;
                    int OppNormalCount=0;

                    //If the support ray intersects the element at the first node of the element
                    if (abs(u)<Epsilon1 || abs(1-u)<Epsilon1)
                    {
                        SupVect.normalize();
                        OppNormalCount=0;
                        //Initialize the node index for the case where abs(u)<Epsilon1 
                        int NodeIndex=0;
                        if (abs(1-u)<Epsilon1)
                            NodeIndex=1;

                        //Test if the adjacents element normals are in line with the element or opposit to it
                        for (int i=0; i<SurfElement[ElNum-1].AttachedNodes[NodeIndex]->AttachedElements.size(); i++)
                        {
                            ElNumTemp=SurfElement[ElNum-1].AttachedNodes[NodeIndex]->AttachedElements[i];
                            ScalarProd=0;
                            for (int j=0; j<Dimension; j++)
                            {
                                ScalarProd+=SurfElement[ElNumTemp-1].Normal[j]*SupVect(j);
                            }
                            //Determine if the support ray exits the domain
                            if (ScalarProd>Epsilon2 && t<0.5 && abs(acos(abs(ScalarProd))-pi/2)>Epsilon3)
                                OppNormalCount++;
                            //Determine if the support ray enters into the domain
                            else if (ScalarProd<Epsilon2 && t>0.5 && abs(acos(abs(ScalarProd))-pi/2)>Epsilon3)
                                OppNormalCount++;
                            if (OppNormalCount==SurfElement[ElNum-1].AttachedNodes[NodeIndex]->AttachedElements.size())
                            {
                                TempResult=true;
                            }
                        }
                    }
                }
            }
        }
    }
    
    return TempResult;
}

double Model::GetSegmentElementAngle(MatrixXd B)
{
    double TempResult=0;
    VectorXd Segment=B.col(0);
    Segment.normalize();
    VectorXd Element1=B.col(1);
    Element1.normalize();
    VectorXd Element2;
    if (Dimension==3)
    {
        Element2=B.col(2);
        Element2.normalize();
    }
    if (Dimension==2)
    {
        //Get the element normal
        double TempVal=Element1(0);
        Element1(0)=Element1(1);
        Element1(1)=TempVal;
        TempResult=Segment.dot(Element1);
    }
    else if (Dimension==3)
    {
        VectorXd ElementNormal;
        ElementNormal.resize(Dimension);
        ElementNormal(0)=Element1(1)*Element2(2)-Element1(2)*Element2(1);
        ElementNormal(1)=Element1(2)*Element2(0)-Element1(0)*Element2(2);
        ElementNormal(2)=Element1(0)*Element2(1)-Element1(1)*Element2(0);
        ElementNormal.normalize();
        TempResult=Segment.dot(ElementNormal);
    }
    return TempResult;
}

bool Model::InDomain(vector<float> PointVect, bool Flag)
{
    bool NodeInSurf=true;
    //Determine the support nodes of the candidate node
    //Create the search query
    Point_3* query=NULL;
    query= new Point_3(PointVect[0],PointVect[1],PointVect[2]);
    Nodes[0].X[0]=PointVect[0];
    Nodes[0].X[1]=PointVect[1];
    Nodes[0].X[2]=PointVect[2];

    Distance tr_dist;
    int k=15;
    long* SupNode=NULL;
    SupNode=new long[k];
    int i=0;

    // search K nearest neighbours
    K_neighbor_search search(tree, *query, k, 0.0,true,tr_dist,true);
    //Determine if a boundary node is in the K nearest neighbours
    bool HasBoundSupNode=false;
    for(K_neighbor_search::iterator it = search.begin(); it != search.end(); it++)
    {
        long SupNodeTemp=boost::get<1>(it->first);
        if (Nodes[SupNodeTemp].BoundaryNode==true)
        {
            HasBoundSupNode=true;
        }
    }
    if (HasBoundSupNode==false)
    {
        NodeInSurf=true;
    }
    else if (CAD_Model_Available==false)
    {
        vector<long> ElementsInSup;
        ElementsInSup.clear();
        //Fill the index and distance array with the values using the iterator
        for(K_neighbor_search::iterator it = search.begin(); it != search.end(); it++)
        {
            SupNode[i] = boost::get<1>(it->first);
            if (Flag==true)
            {
                cout << "SupNode[i]=" << SupNode[i] << endl;
            }

            //Get all the elements in the support
            for (int j=0; j<Nodes[SupNode[i]].AttachedElements.size(); j++)
            {
                ElementsInSup.push_back(Nodes[SupNode[i]].AttachedElements[j]);
            }
            i++;
        }

        //Sort the elements vector and remove the duplicates
        if (ElementsInSup.size()>0)
        {
            sort(ElementsInSup.begin(),ElementsInSup.end());
            ElementsInSup.erase(unique(ElementsInSup.begin(),ElementsInSup.end()),ElementsInSup.end());
        }
        bool LastSupNodeFound=false;
        int SupNodeIndex=0;

        //If the supports include any boundary elements
        if (ElementsInSup.size()>0)
        {
            while (NodeInSurf==true && LastSupNodeFound==false)
            {
                //Loop over all boundary elments and assess if there is any intersection with the considered ray
                for (int j=0; j<2; j++)
                {
                    if (IntersectBoundary(0,SupNode[SupNodeIndex],ElementsInSup[j])==true)
                    {
                        NodeInSurf=false;
                    }
                }
                SupNodeIndex++;
                if (SupNodeIndex==k)
                    LastSupNodeFound=true;
            }
        }

        delete[] SupNode;
        delete query;
    }
    else if (CAD_Model_Available==true)
    {
        double* NewX=NULL;
        NewX=new double[3];
        NewX[0]=PointVect[0];
        NewX[1]=PointVect[1];
        if (Dimension==2)
            NewX[2]=0;
        else
            NewX[2]=PointVect[2];
        delete[] NewX;
    }
    return NodeInSurf;
}
