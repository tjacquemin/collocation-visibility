#include "VTKOutFile.h"

VTKOutFile::VTKOutFile() {
}

VTKOutFile::VTKOutFile(const VTKOutFile& orig) {
}

VTKOutFile::~VTKOutFile() {
}

void VTKOutFile::PrintVTKOutputFile(Model* M, LinearProblem* L, string Path)
{
    std::ofstream *outfile;
    outfile= new std::ofstream(Path.replace(Path.end()-3,Path.end(),"vtk"));
    
    *outfile << "# vtk DataFile Version 1.0" << endl;
    *outfile << Path << endl;
    *outfile << "ASCII" << endl;
    *outfile << endl;
    
    //Print the node coordinates
    *outfile << "DATASET UNSTRUCTURED_GRID" << endl;
    *outfile << "POINTS " << M->NodeNumber << " float" << endl;
    for (int i=0; i<M->NodeNumber; i++)
    {
        *outfile << "  " << M->ToString(M->Nodes[i+1].X[0],5,15,true) << " " << M->ToString(M->Nodes[i+1].X[1],5,15,true);
        if (M->Dimension==2)
            *outfile << " 0" << endl;
        else
            *outfile << " " << M->ToString(M->Nodes[i+1].X[2],5,15,true) << endl;
    }
    
    //Print point data
    *outfile << endl;
    *outfile << "POINT_DATA " << M->NodeNumber << endl;
    
    //Print the values approx, exact and the error
    for (int ii=0; ii<3; ii++)
    {
        //Get the suffix
        string Suffix="";
        if (ii==0)
            Suffix="_Approx";
        else if (ii==1)
            Suffix="_Exact";
        else if (ii==2)
            Suffix="_Error";
        
        //Print the displacement field
        *outfile << "VECTORS U" << Suffix << " float" << endl;  
        for (int i=0; i<M->NodeNumber; i++)
        {
            long RowU1=M->Dimension*i;
            long RowU2=M->Dimension*i+1;
            long RowU3=M->Dimension*i+2;
            double Val1=L->U(RowU1);
            double Val2=L->U(RowU2);
            double Val3=0;
            double Val1Ex=M->ExactU(RowU1);
            double Val2Ex=M->ExactU(RowU2);
            double Val3Ex=0;
            double ValPrint1=0;
            double ValPrint2=0;
            double ValPrint3=0;
            
            if (M->Dimension==3)
            {
                Val3=L->U(RowU3);
                Val3Ex=M->ExactU(RowU3);
            }
            if (ii==0)
            {
                ValPrint1=Val1;
                ValPrint2=Val2;
                ValPrint3=Val3;
            }
            else if (ii==1)
            {
                ValPrint1=Val1Ex;
                ValPrint2=Val2Ex;
                ValPrint3=Val3Ex;
            }
            else if (ii==2)
            {
                ValPrint1=abs(Val1-Val1Ex);
                ValPrint2=abs(Val2-Val2Ex);
                ValPrint3=abs(Val3-Val3Ex);
            }
            
            *outfile << "  " << M->ToString(ValPrint1,5,15,true) << "  " << M->ToString(ValPrint2,5,15,true);
            if (M->Dimension==2)
                *outfile << "  0";
            else if (M->Dimension==3)
                *outfile << "  " << M->ToString(ValPrint3,5,15,true);
            *outfile << endl;
        } 
        
        //Print all the stress components
        for (int i=0; i<3*(M->Dimension-1)+1; i++)
        {
            *outfile << endl;
            string ComponentTitle="";
            long RowS=0;

            if (i==0)
                ComponentTitle="S11";
            else if (i==1)
                ComponentTitle="S12";
            else if (i==3*(M->Dimension-1))
                    ComponentTitle="vonMises_Stress";
            else if (M->Dimension==2)
            {
                if (i==2)
                    ComponentTitle="S22";
            }
            else if (M->Dimension==3)
            {
                if (i==2)
                    ComponentTitle="S13";
                else if (i==3)
                    ComponentTitle="S22";
                else if (i==4)
                    ComponentTitle="S23";
                else if (i==5)
                    ComponentTitle="S33";
            }
            ComponentTitle+=Suffix;
            
            *outfile << endl;
            *outfile << "SCALARS " << ComponentTitle << " float" << endl;
            *outfile << "LOOKUP_TABLE default" << endl;
            for (long j=0; j<M->NodeNumber; j++)
            {
                if (i<3*(M->Dimension-1))
                {
                    RowS=j*3*(M->Dimension-1)+i;
                    double Val=L->S_Ord(RowS);
                    double ValEx=M->ExactS(RowS);
                    double PrintVal=0;
                    if (ii==0)
                        PrintVal=Val;
                    else if (ii==1)
                        PrintVal=ValEx;
                    else if (ii==2)
                        PrintVal=abs(Val-ValEx);
                    *outfile << "  " << M->ToString(PrintVal,5,15,true) << endl;
                }
                else if (i==3*(M->Dimension-1))
                {
                    double ValVMS=0;
                    double ValVMSEx=0;
                    double PrintVal=0;
                    if (M->Dimension==2)
                    {
                        ValVMS=pow(pow(L->S_Ord(j*3*(M->Dimension-1)+0),2)+pow(L->S_Ord(j*3*(M->Dimension-1)+2),2)-L->S_Ord(j*3*(M->Dimension-1)+0)*L->S_Ord(j*3*(M->Dimension-1)+2)+3*pow(L->S_Ord(j*3*(M->Dimension-1)+1),2),0.5);
                        ValVMSEx=pow(pow(M->ExactS(j*3*(M->Dimension-1)+0),2)+pow(M->ExactS(j*3*(M->Dimension-1)+2),2)-M->ExactS(j*3*(M->Dimension-1)+0)*M->ExactS(j*3*(M->Dimension-1)+2)+3*pow(M->ExactS(j*3*(M->Dimension-1)+1),2),0.5);
                    }
                    else if (M->Dimension==3)
                    {
                        ValVMS=pow(0.5*(pow(L->S_Ord(j*3*(M->Dimension-1)+0)-L->S_Ord(j*3*(M->Dimension-1)+3),2)+pow(L->S_Ord(j*3*(M->Dimension-1)+0)-L->S_Ord(j*3*(M->Dimension-1)+5),2)+pow(L->S_Ord(j*3*(M->Dimension-1)+3)-L->S_Ord(j*3*(M->Dimension-1)+5),2)
                            +6*(pow(L->S_Ord(j*3*(M->Dimension-1)+1),2)+pow(L->S_Ord(j*3*(M->Dimension-1)+2),2)+pow(L->S_Ord(j*3*(M->Dimension-1)+4),2))),0.5);
                        ValVMSEx=pow(0.5*(pow(M->ExactS(j*3*(M->Dimension-1)+0)-M->ExactS(j*3*(M->Dimension-1)+3),2)+pow(M->ExactS(j*3*(M->Dimension-1)+0)-M->ExactS(j*3*(M->Dimension-1)+5),2)+pow(M->ExactS(j*3*(M->Dimension-1)+3)-M->ExactS(j*3*(M->Dimension-1)+5),2)
                            +6*(pow(M->ExactS(j*3*(M->Dimension-1)+1),2)+pow(M->ExactS(j*3*(M->Dimension-1)+2),2)+pow(M->ExactS(j*3*(M->Dimension-1)+4),2))),0.5);
                    }
                    if (ii==0)
                        PrintVal=ValVMS;
                    else if (ii==1)
                        PrintVal=ValVMSEx;
                    else if (ii==2)
                        PrintVal=abs(ValVMS-ValVMSEx);
                    *outfile << "  " << M->ToString(PrintVal,5,15,true) << endl;
                }
            }
        }
    }
    
    outfile->close();
    delete outfile;
}