#include "Node.h"

Node::Node() {
}

Node::Node(const Node& orig) {
}

Node::~Node() {
    delete [] X;
    if (BoundaryNode==true)
    {
        delete [] BC_D_Bool;
        delete [] BC_N_Bool;
        delete [] BC_D;
        delete [] BC_N;
        delete [] BC_DOF;
        delete [] LocalBoundaryCondition;
        for (int i=0; i<Dimension;i++)
        {
            delete [] Normal[i];
        }
        delete [] Normal;
    }
    if (HasExactS==true)
        delete [] ExactS;
    if (HasExactU==true)
        delete [] ExactU;
    delete [] SupNodes;
}

void Node::SetNode(long Number, int Dim, double X_1, double X_2, double X_3=0)
{
    Dimension=Dim;
    X = new double[Dim];
    X[0]=X_1;
    X[1]=X_2;
    if (Dim==3) 
    {
        X[2]=X_3;
    }
    NodeNum=Number;
    BoundaryNode=false;
    BC_D_Init=false;
    BC_N_Init=false;
    AttachedElements.clear();
}

long Node::NodeNumber()
{
    return NodeNum;
}

void Node::SetBoundNode(bool IsBound, int Dim, int BC_Dim, int BC_Pos, string BCDOFStr, float BC_Val, float NormalX1=0, float NormalX2=0, float NormalX3=0, string prefix="", string BC_Axis="")
{
    //Nodes[NodeNum].SetBoundNode(true,Dimension,BCDOF,BCIndex,BCDOFStr,BC_Val,NX1,NX2,NX3,"D",BC_Axis);
    if (NodeInitialized==false)
    {
        BC_D_Bool = new bool[Dim];
        BC_N_Bool = new bool[Dim];
        BC_D = new float[Dim];
        BC_N = new float[Dim];
        BC_DOF = new int[Dim];
        Normal = new float*[Dim];
        LocalBoundaryCondition = new bool[Dim];

        for (int i=0; i<Dim; i++)
        {
            BC_D_Bool[i]=false;
            BC_N_Bool[i]=false;
            LocalBoundaryCondition[i]=false;
            Normal[i] = new float[Dim];
        }
        NodeInitialized=true;
    }
    if (BC_Axis=="L")
    {
        LocalBoundaryCondition[BC_Pos]=true;
    }

    if (IsBound == true)
    {
        BoundaryNode=true;
        if (prefix=="D")
        {
            BC_D[BC_Pos]=BC_Val;
            BC_D_Bool[BC_Pos]=true;
        }
        else if (prefix=="N")
        {
            BC_N[BC_Pos]=BC_Val;
            BC_N_Bool[BC_Pos]=true;
        }
    }
    Normal[BC_Pos][0]=NormalX1;
    Normal[BC_Pos][1]=NormalX2;
    if (Dim==3)
    {
        Normal[BC_Pos][2]=NormalX3;
    }
    if (BC_Axis!="L" && BC_Dim==0)
    {
        for (int i=0; i<Dim; i++)
        {
            Normal[i][0]=NormalX1;
            Normal[i][1]=NormalX2;
            if (Dim==3)
            {
                Normal[i][2]=NormalX3;
            }
        }
        
    }
    BC_DOF[BC_Pos]=BC_Dim;
}

void Node::SetSupRadius(float RadiusValue)
{
    SupRadiusSq=RadiusValue;
    SupRadius=pow(SupRadiusSq,0.5);
}

void Node::FreeCoeff()
{
    for (int i=0; i<SupSize; i++)
    {
        delete [] Coeff[i];
    }
    delete [] Coeff;
}
