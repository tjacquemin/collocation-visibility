#include <stdlib.h>
#include "Model.h"

Model::Model() {
}

Model::Model(const Model& orig) {
}

Model::~Model() {
    delete [] Nodes;
}

string Model::split(string str, char delimiter, int Location) 
//Split the path
{
    string internal="";
    stringstream ss(str); // Turn the string into a stream.
    string tok;
    int i=0;
    while (getline(ss, tok, delimiter))
    {
	if (i==Location) 
        {
            internal=tok;
            break;
        }
        i++;
    }
    return trim(internal);
}

string Model::trim(string str)
//Function Trim (remove spaces from name start and end)
{
    string::size_type pos = str.find_last_not_of('\r');
    if (pos != string::npos) {str.erase(pos + 1); }
    pos = str.find_last_not_of(' ');
    if (pos != string::npos) {str.erase(pos + 1); }
    pos = str.find_first_not_of(' ');
    if (pos != string::npos) {str.erase(0, pos); }
    return str;
}

void Model::SetThreadNumber(int ThreadNum)
{
    ThreadNumber=ThreadNum;
}

void Model::ProcessMemUsage(double& vm_usage, double& resident_set)
{
   using std::ios_base;
   using std::ifstream;
   using std::string;

   vm_usage     = 0.0;
   resident_set = 0.0;

   // 'file' stat seems to give the most reliable results
   //
   ifstream stat_stream("/proc/self/stat",ios_base::in);

   // dummy vars for leading entries in stat that we don't care about
   //
   string pid, comm, state, ppid, pgrp, session, tty_nr;
   string tpgid, flags, minflt, cminflt, majflt, cmajflt;
   string utime, stime, cutime, cstime, priority, nice;
   string O, itrealvalue, starttime;

   // the two fields we want
   //
   unsigned long vsize;
   long rss;

   stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
               >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
               >> utime >> stime >> cutime >> cstime >> priority >> nice
               >> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

   stat_stream.close();

   long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
   vm_usage     = vsize / 1024.0/1000;
   resident_set = rss * page_size_kb/1000;
}

void Model::Print(string StringVal, bool MemoryDisplay)
{
    const char* CharVal;
    CharVal=StringVal.c_str();
    cout << " " ;
    if (MemoryDisplay==false)
    {
        int CharLength=StringVal.length();
        int j=0, RowCount=0;
        for(int i=0; i<CharLength; i++)
        {
            cout << CharVal[i];
            j++;
            if (j>=MaxRowWidth)
            {
                cout << "" <<endl;
                cout << " ";
                j=0;
                RowCount++;
            }
        }
        cout << "" <<endl;
        if (RowCount>0) cout << "" <<endl;
    }
    else
    {
        double vm, rss;
        ProcessMemUsage(vm, rss);
        cout << left << setw(MaxRowWidth);
        cout << StringVal;
        cout << setw(7);
        cout << right << setprecision(1) << fixed << vm << " MB" << endl;
    }
    
}

void Model::PrintLoading(long i, long total, string StringVal, bool MemoryDisplay)
{
    int PrintInc=total/500+1;
    if (i % PrintInc == 0 && i!= total)
    {
        double Percentage=(double)i/total*100;
        cout << " " << StringVal << setprecision(1) <<  Percentage << "%";
        cout << '\r';
    }
    else if (i== total)
    {
        Print(StringVal+"100%", true);
    }
}

void Model::GetStartEndNode(int ThreadIndex, int TreadNum, long& Start, long& End)
{
    for (int i=0; i< TreadNum; i++)
    {
        if (ThreadIndex==i)
        {
            Start=(int)((NodeNumber*ThreadIndex)/TreadNum)+1;
            End=(int)((NodeNumber*(ThreadIndex+1))/TreadNum);
        }
        if (ThreadIndex==0){Start=1;}
        if (ThreadIndex==TreadNum-1){End=NodeNumber;}
    } 
}

string Model::ToString(double Value, int Precision, int SpaceSize, bool Last)
{
    std::stringstream out;
    string TempStr;
    int Len;
    if (Precision==0)
    {
        out << Value;
        Len=10;
    }
    else
    {
        out.precision(Precision);
        out << std::scientific;
        out << Value;
        Len=SpaceSize;
    }
    TempStr=out.str();
    if (Value >= 0)
    {
        TempStr=" " + TempStr;
    }
    if (Last==false)
    {
        TempStr=TempStr + ",";
    }
    while (TempStr.size()<Len && Last==false)
    {
        TempStr=TempStr + " ";
    }
    return TempStr;
}

void Model::ComputeCharactLength()
{
    if (Dimension==2)
    {
        for (long i=1; i<= NodeNumber; i++)
        {
            Nodes[i].CharactLength=pow(Nodes[i].SupRadiusSq*pi,0.5)*pow(Nodes[i].SupSize,-1/Dimension);
        }
    }
    else if (Dimension==3)
    {
        for (long i=1; i<= NodeNumber; i++)
        {
            Nodes[i].CharactLength=pow(Nodes[i].SupRadiusSq,0.5)*pow(4*pi/3,1/Dimension)*pow(Nodes[i].SupSize,-1/Dimension);
        }
    }
    for (long i=1; i<= NodeNumber; i++)
    {
        float TempVal=1;
        for (int j=0; j<Dimension; j++)
        {
            TempVal=TempVal+Nodes[i].X[j];
        }
        Nodes[i].ProdN=TempVal;
    }
}

double Model::ErrorNorm(string ErrorType, string ErrorNormalization, VectorXd RefereSolution, VectorXd ApproxSolution, int ValIndex, int Inc)
{
    double ErrorVal=0;
    double NormalizationVal=1;
    
    //Initialize the normalization
    if (ErrorNormalization=="WEIGHTED")
    {
        NormalizationVal=NodeNumber;
    }
    else if (ErrorNormalization=="RELATIVE")
    {
        NormalizationVal=0;
    }
    
    //Compute the error norm
    for (long i=0; i<NodeNumber; i++)
    {
        double Val=0;
        double Ref=0;
        //Get the difference for normal stress or displacement
        if (ValIndex<3*(Dimension-1))
        {
            Val=ApproxSolution(i*Inc+ValIndex);
            Ref=RefereSolution(i*Inc+ValIndex);
        }
        //Get the difference for von Mises stress components
        else if (ValIndex==3*(Dimension-1))
        {
            if (Dimension==2 && ModelType=="2D_PLANE_STRESS")
            {
                Val=pow(pow(ApproxSolution(i*Inc+0),2)+pow(ApproxSolution(i*Inc+2),2)-ApproxSolution(i*Inc+0)*ApproxSolution(i*Inc+2)+3*pow(ApproxSolution(i*Inc+1),2),0.5);
                Ref=pow(pow(RefereSolution(i*Inc+0),2)+pow(RefereSolution(i*Inc+2),2)-RefereSolution(i*Inc+0)*RefereSolution(i*Inc+2)+3*pow(RefereSolution(i*Inc+1),2),0.5);
            }
            else if (Dimension==2 && ModelType=="2D_PLANE_STRAIN")
            {
                double d1=E*(1-nu)/((1+nu)*(1-2*nu));
                double d2=E*nu/((1+nu)*(1-2*nu));
                double E22_Val=(ApproxSolution(i*Inc+2)-d2/d1*ApproxSolution(i*Inc+0))/(d1-pow(d2,2)/d1);
                double E11_Val=(ApproxSolution(i*Inc+0)-d2*E22_Val)/d1;
                double E22_Ref=(RefereSolution(i*Inc+2)-d2/d1*RefereSolution(i*Inc+0))/(d1-pow(d2,2)/d1);
                double E11_Ref=(RefereSolution(i*Inc+0)-d2*E22_Ref)/d1;
                double S33_Val=d2*(E11_Val+E22_Val);
                double S33_Ref=d2*(E11_Ref+E22_Ref);
                Val=pow(0.5*(pow(ApproxSolution(i*Inc+0)-ApproxSolution(i*Inc+2),2)+pow(ApproxSolution(i*Inc+0)-S33_Val,2)+pow(ApproxSolution(i*Inc+2)-S33_Val,2)
                        +6*pow(ApproxSolution(i*Inc+1),2)),0.5);
                Ref=pow(0.5*(pow(RefereSolution(i*Inc+0)-RefereSolution(i*Inc+2),2)+pow(RefereSolution(i*Inc+0)-S33_Ref,2)+pow(RefereSolution(i*Inc+2)-S33_Ref,2)
                        +6*pow(RefereSolution(i*Inc+1),2)),0.5);
            }
            else if (Dimension==3)
            {
                Val=pow(0.5*(pow(ApproxSolution(i*Inc+0)-ApproxSolution(i*Inc+3),2)+pow(ApproxSolution(i*Inc+0)-ApproxSolution(i*Inc+5),2)+pow(ApproxSolution(i*Inc+3)-ApproxSolution(i*Inc+5),2)
                        +6*(pow(ApproxSolution(i*Inc+1),2)+pow(ApproxSolution(i*Inc+2),2)+pow(ApproxSolution(i*Inc+4),2))),0.5);
                Ref=pow(0.5*(pow(RefereSolution(i*Inc+0)-RefereSolution(i*Inc+3),2)+pow(RefereSolution(i*Inc+0)-RefereSolution(i*Inc+5),2)+pow(RefereSolution(i*Inc+3)-RefereSolution(i*Inc+5),2)
                        +6*(pow(RefereSolution(i*Inc+1),2)+pow(RefereSolution(i*Inc+2),2)+pow(RefereSolution(i*Inc+4),2))),0.5);
            }
        }
        
        if (ErrorType=="L1_ERROR")
        {
            ErrorVal+=abs(Val-Ref);
        }
        else if (ErrorType=="L2_ERROR")
        {
            ErrorVal+=pow(Val-Ref,2);
        }
        else if (ErrorType=="LInf_ERROR")
        {
            if (abs(Val-Ref)>ErrorVal)
                ErrorVal=abs(Val-Ref);
        }
        if (ErrorNormalization=="RELATIVE")
        {
            NormalizationVal+=pow(Ref,2);
        }   
    }
    
    //Take the square root for the L2 norms
    if (ErrorType=="L2_ERROR")
        ErrorVal=pow(ErrorVal,0.5);
    
    //Nomalize the error
    if (ErrorNormalization=="RELATIVE")
    {
        NormalizationVal=pow(NormalizationVal,0.5);
    }
    ErrorVal=ErrorVal/NormalizationVal;
    return ErrorVal;
}