#include "Model.h"

void Model::LoadSupportCGAL()
{   
    Print("Initializing the node tree.",true);
    LoadTree();
    
    if (VisibilityType=="DIRECT_SURF")
    {
        //Load the elements connected to the nodes
        LoadConnectedElements();
    }
    
    #pragma omp parallel num_threads(ThreadNumber) 
    {
        long* IndexArrayLoc=NULL;
        double* DistArrayLoc=NULL;
    
        int ThreadNum=omp_get_thread_num();
        long Start, End;
        //Get the start and end node of the thread
        GetStartEndNode(ThreadNum,ThreadNumber,Start, End);
        
        for (long i=Start; i<=End; i++)
        {
            float SupScaling=3.0;
            bool SupLoaded=false;
            while (SupLoaded==false)
            {
                int k=0, SupSize=0;
                if (Nodes[i].BoundaryNode==true)
                {
                    k=SupSizeBound*SupScaling;
                }
                else
                {
                    k=SupSizeInt*SupScaling;
                }
                
                IndexArrayLoc=new long[k];
                DistArrayLoc=new double[k];

                GetNeighbourNodes(SupScaling,i,&k,&SupSize,IndexArrayLoc,DistArrayLoc);
                //Determine is a boundary node is included in the support
                bool HasBCNode=false;
                for(int j=0; j<k; j++)
                {
                    if (Nodes[IndexArrayLoc[j]].BoundaryNode==true)
                    {
                        HasBCNode=true;
                    }
                }
                if (HasBCNode==true && VisibilityType=="DIRECT")
                {
                    LoadVisibilitySupport(i,IndexArrayLoc,DistArrayLoc,k, SupSize);
                }
                else if (HasBCNode==true && VisibilityType=="DIRECT_SURF")
                {
                    LoadVisibilitySupport2D(i,IndexArrayLoc,DistArrayLoc,k, SupSize);
                }
                else if (StencilSelection=="AREA")
                {
                    LoadAreaSupport(i,IndexArrayLoc,DistArrayLoc,k, SupSize,SupScaling);
                }
                else
                {
                    LoadNormalSupport(i,IndexArrayLoc,DistArrayLoc,k, SupSize);
                }
                delete[] DistArrayLoc;
                delete[] IndexArrayLoc;
                SupLoaded=true;
                SupScaling++;
            }
        }
    }
    
    LoadCoefficients();
    //Compute the characteristic length if stabilization requested
    if (Stabilization==true)
    {
        ComputeCharactLength();
    }
    Print("Supports successfully loaded.",true);
}

void Model::LoadTree()
{
    MaxSupSize=0;
    MinSupSize=SupSizeInt*10;
    
    long LastNodeNum=0;
    if (IncludeSingularNodes==false && VisibilityType=="DIRECT")
    {
        LastNodeNum=NodeNumber+SingularNodeNumber;
    }
    else
    {
        LastNodeNum=NodeNumber;
    }
    
    std::vector<Point_3> points(LastNodeNum);
    std::vector<long> indices(LastNodeNum);
    
    for (long i=1; i<=LastNodeNum; i++)
    {
        if (Dimension==2)
        {
            points[i-1]=Point_3(Nodes[i].X[0],Nodes[i].X[1],0.0);
        }
        else if (Dimension==3)
        {
            points[i-1]=Point_3(Nodes[i].X[0],Nodes[i].X[1],Nodes[i].X[2]);
        }
        indices[i-1]=i;
    }
    
    // Insert number_of_data_points in the trees
    tree.insert(
    boost::make_zip_iterator(boost::make_tuple(points.begin(),indices.begin())),
    boost::make_zip_iterator(boost::make_tuple(points.end(),indices.end())));
}

void Model::GetNeighbourNodes(float SupScaling, long i, int* k, int* SupSize, long* IndexArrayLoc, double* DistArrayLoc)
{
    if (Nodes[i].BoundaryNode==true)
    {
        *k=SupSizeBound*SupScaling;
        *SupSize=SupSizeBound;
    }
    else
    {
        *k=SupSizeInt*SupScaling;
        *SupSize=SupSizeInt;
    }

    //Create the search query
    Point_3* query=NULL;
    if (Dimension==2)
    {
        if (Nodes[i].CollocationPointOffset==false)
        {
            query= new Point_3(Nodes[i].X[0], Nodes[i].X[1], 0.0);
        }
        else
        {
            query= new Point_3(Nodes[i].CollocX[0], Nodes[i].CollocX[1], 0.0);
        }
    }
    else
    {
        query= new Point_3(Nodes[i].X[0], Nodes[i].X[1], Nodes[i].X[2]);
    }
        
    Distance tr_dist;
    // search K nearest neighbours
    K_neighbor_search search(tree, *query, *k, 0.0,true,tr_dist,true);

    int j=0;
    //Fill the index and distance array with the values using the iterator
    for(K_neighbor_search::iterator it = search.begin(); it != search.end(); it++)
    {
        IndexArrayLoc[j] = boost::get<1>(it->first);
        DistArrayLoc[j] = tr_dist.inverse_of_transformed_distance(it->second);
        j++;
    }
    delete query;
}

void Model::LoadNormalSupport(long i,long* IndexArray,double* DistArray,int k, int SupSize)
{
    double SupRad = DistArray[SupSize-1]*1.001;
    Nodes[i].SupRadiusSq=pow(SupRad,2);
    Nodes[i].SupRadius=SupRad;
    
    int CountSup=0;
    int j=0;
    while (DistArray[j]<=SupRad && j<k)
    {
        CountSup++;
        j++;
    }
    Nodes[i].SupSize=CountSup;
    Nodes[i].SupNodes = new long[CountSup];
    j=0;
    for (j=0;j<CountSup;j++)
    {
        Nodes[i].SupNodes[j]=IndexArray[j];
    }
}

void Model::LoadAreaSupport(long i,long* IndexArray,double* DistArray,int k, int SupSize,int SupScaling)
{
    double SupRad = DistArray[SupSize-1]*1.001;
    Nodes[i].SetSupRadius(pow(SupRad,2));
    
    //Get the number of triangles associated to the "ideal" discretization
    double nTrianglesEqui=(1.27E-2 * pow(SupSize,2) + 1.07 * SupSize - 8.62E-1);
    double aTrianglesEqui=pow(pi*pow(SupRad,2)*4/(nTrianglesEqui*pow(3,0.5)),0.5);
    
    int CountSup=0;
    int j=0;
    while (DistArray[j]<=SupRad && j<k)
    {
        CountSup++;
        j++;
    }
    Nodes[i].SupSize=CountSup;
    Nodes[i].SupNodes = new long[CountSup];
    
    j=0;
    int SelectedNode=0;
    for (j=0;j<SupScaling*SupSize;j++)
    {
        float MinDist=-1;
        
        for (int k=0; k<SelectedNode; k++)
        {
            float LocDist=0;
            for (int l=0; l<Dimension; l++)
            {
                LocDist+=pow(Nodes[IndexArray[j]].X[l]-Nodes[Nodes[i].SupNodes[k]].X[l],2);
            }
            LocDist=pow(LocDist,0.5);
            if (k==0)
            {
                MinDist=LocDist;
            }
            else
            {
                if(LocDist < MinDist)
                    MinDist=LocDist;
            }
        }
        if (MinDist>aTrianglesEqui*StencilSelectionThreshold || MinDist==-1)
        {
            Nodes[i].SupNodes[SelectedNode]=IndexArray[j];
            SelectedNode++;
        }
        if (SelectedNode==CountSup)
            break;
    }
}

void Model::LoadCoefficients()
{
    for (long i=1; i<=NodeNumber; i++)
    {
        int CountSup=Nodes[i].SupSize;
        //Initialize the Coeff array if an estimator is requested by the user
        int DeriveNum=0;
        if (ErrEstimator==true || WeakBC==true || Method == "DCPSE0" || Method == "DCPSE1" || Method == "DCPSE2" || Method == "GFD")
        {
            Nodes[i].Coeff = new double *[CountSup+1];
            if (Nodes[i].CollocationPointOffset==true)
            {
                DeriveNum=6;
            }
            else
            {
                DeriveNum=5;
            }
            if (Dimension==3)
            {
                DeriveNum=9;
            }
            for (int ii=0; ii<CountSup+1; ii++)
            {
                Nodes[i].Coeff[ii] = new double[DeriveNum];
            }
        }

        //Update the min and max support size if necessary
        if (CountSup > MaxSupSize) MaxSupSize=CountSup;
        if (CountSup < MinSupSize) MinSupSize=CountSup;
    }
}
