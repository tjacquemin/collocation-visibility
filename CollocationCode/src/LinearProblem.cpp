#include "LinearProblem.h"

LinearProblem::LinearProblem() {
}

LinearProblem::LinearProblem(const LinearProblem& orig) {
}

LinearProblem::~LinearProblem() {
    delete [] F_Vec;
}

void LinearProblem::Build(Model* M)
{   
    BuildInitialization(M);
    
    //Set the number of Thread to be used (user input)
    int TotalThreadNumber=M->ThreadNumber;
    long *TripStressTreadStartInc = new long[TotalThreadNumber];
    long *TripStiffTreadStartInc = new long[TotalThreadNumber];
    long *TripLocTreadStartInc = new long[TotalThreadNumber];
    GetTripTreadStartIndices(M,TripStressTreadStartInc,TripStiffTreadStartInc,TripLocTreadStartInc);
    
    //Initialize the first triplet array dimension
    TripStiff = new T[TripStiffLength+AdditionalDOFs+AdditionalForceVectDOFs];
    TripStress = new T[TripStressLength];
    
    F_Vec = new double[M->NodeNumber*M->Dimension+AdditionalForceVectDOFs];
    
    M->Print("Problem Memory Allocation.",true);
    
    //Create the problem classes and load the triplet vectors
    if (M->Method == "GFD")
    {
        GFD_Problem M_Temp;
        M_Temp.InitializeProblem(M,TripStressTreadStartInc,TripStiffTreadStartInc);
        //Load the derivative coefficients first if a weak for is used
        M->Print("Loading the GFD derivative coefficientes.",true);
        #pragma omp parallel num_threads(TotalThreadNumber) 
        {
            M_Temp.LoadDerivativeCoefficients(M,omp_get_thread_num(),TotalThreadNumber);
        }
        
        //Load the Striffness and the Stress triplets
        M->Print("Loading the Triplets.",true);
        #pragma omp parallel num_threads(TotalThreadNumber) 
        {
            M_Temp.LoadVectors(M,F_Vec,TripStiff,TripStress,omp_get_thread_num(),TotalThreadNumber);
        }
    }  
    
    //Load Lagrange triplets entries if necessary
    if (M->BCTreatType=="LAGRANGE")
    {
        LoadLagrangeTriplets(M);
    }
    
    M->Print("Triplets Loaded.",true);
    TestTriplets(M);
    
    //Delete the Derivative Coefficients
    if ((M->Method == "DCPSE0" || M->Method == "DCPSE1" || M->Method == "DCPSE2" || M->Method == "GFD") && M->ErrEstimator==false)
    {
        for (long i=1; i<=M->NodeNumber; i++)
        {
            M->Nodes[i].FreeCoeff();
        }
    }
    
    delete [] TripStressTreadStartInc;
    delete [] TripStiffTreadStartInc;
    delete [] TripLocTreadStartInc;
}

void LinearProblem::TestTriplets(Model* M)
{
    bool* TestCol;TestCol=new bool[M->Dimension*M->NodeNumber+AdditionalForceVectDOFs];
    bool* TestRow;TestRow=new bool[M->Dimension*M->NodeNumber+AdditionalForceVectDOFs];
    for (long i=0; i<M->Dimension*M->NodeNumber+AdditionalForceVectDOFs; i++)
    {
        TestCol[i]=false;TestRow[i]=false;
    }
    
    //Verify that the triplet is composed of continuous rows
    for (long i=0; i< TripStiffLength+AdditionalDOFs+AdditionalForceVectDOFs; i++)
    {
        if (TripStiff[i].col()<M->Dimension*M->NodeNumber)
            TestCol[TripStiff[i].col()]=true;
        else
            cout << " Error Row" << TripStiff[i].row() << ": the column is out of the matrix " << TripStiff[i].col() << endl;
        if (TripStiff[i].col()<M->Dimension*M->NodeNumber)
            TestRow[TripStiff[i].row()]=true;
        else
            cout << " Error Row" << TripStiff[i].row() << ": the row is out of the matrix." << endl;
        if (i>0 && M->Method != "IMLS1" && M->Method != "MLS1" && M->Method != "IMLS2" && M->Method != "MLS2" && M->Method != "RBF_FD")
        {
            if (TripStiff[i-1].row()>TripStiff[i].row() || TripStiff[i-1].row()+1<TripStiff[i].row() || TripStiff[i].row()< 0 || TripStiff[i].col()<0)
            {
                cout << " Error Row:" << i << ", TripRow=" << TripStiff[i].row() << ", TripCol=" << TripStiff[i].col() << ", TripVal=" << TripStiff[i].value() << endl;
            }
        }
    }
    
    for (long i=0; i<M->Dimension*M->NodeNumber+AdditionalForceVectDOFs; i++)
    {
        if (TestRow[i]==false)
        {
            cout << " Error Empty Row " << i << endl;
        }
        if (TestCol[i]==false)
        {
            cout << " Error Empty Col " << i << endl;
        }
    }
    delete [] TestCol;delete [] TestRow;
}

void LinearProblem::BuildInitialization(Model* M)
{
    if (M->WeakBC==true) 
        GetWeakFormSup(M);
    
    if (M->ModelType=="2D_PLANE_STRAIN")
    {
        d1=M->E*(1-M->nu)/((1+M->nu)*(1-2*M->nu));
        d2=M->E*M->nu/((1+M->nu)*(1-2*M->nu));
        d3=M->E*(1-2*M->nu)/((1+M->nu)*(1-2*M->nu));
    }
    else if (M->ModelType=="2D_PLANE_STRESS")
    {
        d1=M->E/(1-pow(M->nu,2));
        d2=M->E*M->nu/(1-pow(M->nu,2));
        d3=M->E*(1-M->nu)/(1-pow(M->nu,2));
    }
    else
    {
        d1=M->E/(1-pow(M->nu,2));
        d2=M->E*M->nu/(1-pow(M->nu,2));
        d3=M->E*(1-M->nu)/(1-pow(M->nu,2));
    }
}

void LinearProblem::GetTripTreadStartIndices(Model* M, long* TripStressTreadStartInc, long* TripStiffTreadStartInc, long* TripLocTreadStartInc)
{
    int TotalThreadNumber=M->ThreadNumber;
    int ThreadInc=0;
    int TripSizeMult;
    if (M->Dimension==2)
    {
        TripSizeMult=6;
    }
    else if (M->Dimension==3)
    {
        TripSizeMult=15;
    }
    
    //Get the start index of each thread
    TripStiffLength=0;TripStressLength=0;TripLocLength=0;
    for (long i=0; i<M->NodeNumber; i++)
    {
        if ((int)((M->NodeNumber*ThreadInc)/TotalThreadNumber)==i)
        {
            TripStressTreadStartInc[ThreadInc]=TripStressLength;
            TripStiffTreadStartInc[ThreadInc]=TripStiffLength;
            TripLocTreadStartInc[ThreadInc]=TripLocLength;
            ThreadInc++;
        }
        
        TripStressLength=TripStressLength+M->Nodes[i+1].SupSize*TripSizeMult;
        TripLocLength=TripLocLength+M->Nodes[i+1].SupSize*M->Dimension*M->Dimension;
        for (int j=0; j<M->Dimension; j++)
        {
            if (M->Nodes[i+1].BoundaryNode==true)
            {
                if (M->Nodes[i+1].BC_D_Bool[j]==true && M->Method != "IMLS1" && M->Method != "MLS1" && M->Method != "MLS" && M->Method != "IMLS" && M->BCTreatType!="LAGRANGE" && M->WeakBC==false)
                {
                    if (M->Nodes[i+1].LocalBoundaryCondition[j]==true)
                        TripStiffLength+=M->Dimension;
                    else
                        TripStiffLength++;
                }
                else
                {
                    TripStiffLength+=M->Nodes[i+1].SupSize*M->Dimension;
                }
            }
            else if (M->WeakBC==true && M->Nodes[i+1].BoundaryNode==false)
            {
                TripStiffLength+=M->Nodes[i+1].WeakSupSize*M->Dimension;
            }
            else
            {
                TripStiffLength+=M->Nodes[i+1].SupSize*M->Dimension;
            }
        }
        //Count the number of additional DOFs to the force vector and stiffness matrix
        if (M->BCTreatType=="LAGRANGE")
        {
//            if (M->Nodes[i+1].BoundaryNode==true)
            {
                if (M->WeakBC==false)
                {
                    for (int j=0; j<M->Dimension; j++)
                    {
                        if(M->Nodes[i+1].BC_D_Bool[j]==true)
                        {
                            AdditionalDOFs++;
                            AdditionalForceVectDOFs++;
                        }
                    }
                }
                else if (M->WeakBC==true)
                {
                    if (M->Nodes[i+1].BoundaryNode==true) 
                        M->Nodes[i+1].LagrangeMultCol=new long[M->Dimension];
                    for (int j=0; j<M->Dimension; j++)
                    {
                        //Add additional DOFs to the Force Vector based on the Number of Lagrange Multiplers
                        if (M->Nodes[i+1].BoundaryNode==true) 
                        {
                            if (M->Nodes[i+1].BC_D_Bool[j]==true)
                            {
                                M->Nodes[i+1].LagrangeMultCol[j]=M->NodeNumber*M->Dimension+AdditionalForceVectDOFs;
                                AdditionalForceVectDOFs++;
                            }
                        }
                        //Count the number of additional entries to the stiffness matrix in order
                        //to account for the lagrange multiplies in the weak form
                        for (int jj=0; jj<M->Nodes[i+1].SupSize; jj++)
                        {
                            long SupNode=M->Nodes[i+1].SupNodes[jj];
                            if (M->Nodes[SupNode].BoundaryNode==true)
                            {
                            if(M->Nodes[SupNode].BC_D_Bool[j]==true) AdditionalDOFs++;
                            }
                        }
                    }
                }
            }
        }
    }
}

void LinearProblem::LoadMatrices(Model* M)
{
    FillStressMat_Eigen(M);
    U.resize(M->NodeNumber*M->Dimension);
    //Delete the triplet array
    delete [] TripStress;
}

void LinearProblem::FillStressMat_Eigen(Model* M)
{
    long SizeStress;
    int TripSizeMult;
    long SizeStiff=M->NodeNumber*M->Dimension;
    if (M->Dimension==2)
    {
        SizeStress=3*M->NodeNumber;
        TripSizeMult=6;
    }
    else if (M->Dimension==3)
    {
        SizeStress=6*M->NodeNumber;
        TripSizeMult=15;
    }
    StressMat_Eigen.resize(SizeStress,SizeStiff);
    //Fill the triplet vector matrix
    vector<T> StressMatTrip;
    StressMatTrip.resize(TripStressLength);
    for (long i=0; i<TripStressLength ; i++)
    {
        StressMatTrip[i]=TripStress[i];
    }

    M->Print("Starting filling Stress matrix.",true);
    StressMat_Eigen.setFromTriplets(StressMatTrip.begin(), StressMatTrip.end());
    M->Print("Stress matrix filled.",true);
}

void LinearProblem::FillLocMat_Eigen(Model* M)
{
    //Fill the triplet vector matrix
    vector<T> LocMatTrip;
    LocMatTrip.resize(TripLocLength);
    long count2=0;
    for (int i=0; i<TripLocLength ; i++)
    {
        LocMatTrip[count2]=TripLoc[i];
        count2++;                        
    }
    LocMat_Eigen.setFromTriplets(LocMatTrip.begin(), LocMatTrip.end());
}

void LinearProblem::ComputeSolution(Model* M)
{
    S=StressMat_Eigen*U;
    if (M->Dimension==3)
        ReorderS(M);
    else
        S_Ord=S;
}

void LinearProblem::ReorderS(Model* M)
{
    S_Ord.resize(M->NodeNumber*3*(M->Dimension-1));
    //Reorder the vector S to obtain the same stress components order as the input and output files
    for (long i=0; i<M->NodeNumber; i++)
    {
        double S11=S(i*3*(M->Dimension-1)+0);
        double S22=S(i*3*(M->Dimension-1)+1);
        double S33=S(i*3*(M->Dimension-1)+2);
        double S12=S(i*3*(M->Dimension-1)+3);
        double S13=S(i*3*(M->Dimension-1)+4);
        double S23=S(i*3*(M->Dimension-1)+5);
        S_Ord(i*3*(M->Dimension-1)+0)=S11;
        S_Ord(i*3*(M->Dimension-1)+1)=S12;
        S_Ord(i*3*(M->Dimension-1)+2)=S13;
        S_Ord(i*3*(M->Dimension-1)+3)=S22;
        S_Ord(i*3*(M->Dimension-1)+4)=S23;
        S_Ord(i*3*(M->Dimension-1)+5)=S33;
    }
}

void LinearProblem::SetConvergenceReason(KSPConvergedReason IterationStopReason)
{
    if (IterationStopReason==KSP_DIVERGED_INDEFINITE_PC)
    {
        IterationStopReason_Str="Divergence because of indefinite preconditioner";
    }
    else if (IterationStopReason==KSP_DIVERGED_ITS)
    {
        IterationStopReason_Str="Solver ran out of iterations before any convergence.";
    }
    else if (IterationStopReason==KSP_DIVERGED_BREAKDOWN)
    {
        IterationStopReason_Str="The solver could not continue to enlarge the Krylov space. This could be due to a singlular matrix or preconditioner.";
    }
    else if (IterationStopReason<0)
    {
        IterationStopReason_Str="Divergence of the solution.";
    }
    else if (IterationStopReason>0)
    {
        IterationStopReason_Str="Successful resolution of the system. Convergence reason=" + to_string(IterationStopReason) + ".";
    }
}

void LinearProblem::LoadLagrangeTriplets(Model* M)
{
    long count=0;
    for (long i=0; i<M->NodeNumber; i++)
    {
        if (M->Nodes[i+1].BoundaryNode==true)
        {
            for (int j=0; j<M->Dimension; j++)
            {
                if(M->Nodes[i+1].BC_D_Bool[j]==true)
                {
                    float TempVal=1.0;
                    if (M->WeakBC==true)
                    {
                        TripStiff[TripStiffLength+AdditionalDOFs+count]=T(M->NodeNumber*M->Dimension+count,i*M->Dimension+j,TempVal);count++;
                        F_Vec[M->Dimension*M->NodeNumber+count-1]=M->Nodes[i+1].BC_D[j];
                    }
                    else if (M->WeakBC==false)
                    {
                        float TempVal=1.0;
                        TripStiff[TripStiffLength+AdditionalDOFs+count]=T(M->NodeNumber*M->Dimension+count,i*M->Dimension+j,TempVal);count++;
                        F_Vec[M->Dimension*M->NodeNumber+count-1]=M->Nodes[i+1].BC_D[j];
                    }
                }
            }
        }
    }
}

void LinearProblem::GetWeakFormSup(Model* M)
{
    for (long i=1; i<=M->NodeNumber; i++)
    {
//        if(M->Nodes[i].BoundaryNode==true)
//        {
            //Define a temporary weak support vector
            vector <long> WeakSupVect;
            //Get the maximum size of the weak support
            int MaxWeakSupSize=0;
            long SupNode;
            for (int j=0; j<M->Nodes[i].SupSize; j++)
            {
                SupNode=M->Nodes[i].SupNodes[j];
                MaxWeakSupSize=MaxWeakSupSize+M->Nodes[SupNode].SupSize;
            }
            WeakSupVect.resize(MaxWeakSupSize);
            //Initialize the values of the temp weak support
            int count=0;
            for (int j=0; j<M->Nodes[i].SupSize; j++)
            {
                SupNode=M->Nodes[i].SupNodes[j];
                for (int k=0; k<M->Nodes[SupNode].SupSize; k++)
                {
                    WeakSupVect[count]=M->Nodes[SupNode].SupNodes[k];
                    count++;
                }
            }
            //Sort the weak sup vect
            sort(WeakSupVect.begin(), WeakSupVect.end());
            //Get the number of weak sup nodes
            count=1;
            for (int j=0; j<MaxWeakSupSize-1; j++)
            {
                if (WeakSupVect[j] != WeakSupVect[j+1]) count++;
            }
            //Load the node weak support
            M->Nodes[i].WeakSupSize=count;
            M->Nodes[i].WeakSupport=new long[count];
            count=0;
            M->Nodes[i].WeakSupport[count]=WeakSupVect[0];
            for (int j=0; j<MaxWeakSupSize-1; j++)
            {
                if (WeakSupVect[j] != WeakSupVect[j+1])
                {
                    M->Nodes[i].WeakSupport[count+1]=WeakSupVect[j+1];
                    count++;
                }
            }
            WeakSupVect.clear();
//        }
    }
}