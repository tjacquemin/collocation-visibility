#include "Element.h"

Element::Element() {
}

Element::Element(const Element& orig) {
}

Element::~Element() {
}

void Element::NewElement(long ElNumber, string ElType, double* Norm)
{
    ElNum=ElNumber;
    ElementType=ElType;
    if (ElementType=="2D_LINEAR" || ElementType=="2D_QUAD")
    {
        NumbNodes=2;
        Dim=2;
    }
    else if (ElementType=="3D_LINEAR")
    {
        NumbNodes=3;
        Dim=3;
    }
    Normal=new double[Dim];
    double NormalLen=0;
    for (int i=0; i<Dim; i++)
    {
        NormalLen+=pow(Norm[i],2);
    }
    NormalLen=pow(NormalLen,0.5);
    for (int i=0; i<Dim; i++)
    {
        Normal[i]=Norm[i]/NormalLen;
    }
    //Normalize the normal
    AttachedNodes=new Node*[NumbNodes];
}

void Element::LoadConnectionsToNodes()
{
    for (int i=0; i<NumbNodes; i++)
    {
        AttachedNodes[i]->AttachedElements.push_back(ElNum);
    }
}

void Element::LoadCenterNode()
{
    if (ElementType!="2D_QUAD")
    {
        double* X_Center=NULL;
        X_Center = new double[Dim];
        //Initialize the center coordinates
        for (int i=0; i<Dim; i++)
        {
            X_Center[i]=0;
        }
        //Calculate the average coordinates
        for (int i=0; i<Dim; i++)
        {
            for (int j=0; j<NumbNodes; j++)
            {
                X_Center[i]+=AttachedNodes[i]->X[i];
            }
            X_Center[i]=X_Center[i]/NumbNodes;
        }
        CenterNode.SetNode(0,Dim,X_Center[0],X_Center[1],X_Center[2]);
    }
}