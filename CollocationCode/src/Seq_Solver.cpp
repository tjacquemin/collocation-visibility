#include "Seq_Solver.h"

Seq_Solver::Seq_Solver() {
}

Seq_Solver::Seq_Solver(const Seq_Solver& orig) {
}

Seq_Solver::~Seq_Solver() {
}

void Seq_Solver::Solve(Model* M, LinearProblem* L)
{
    FillStiffMat_Petsc(M,L);
        
    int Dimension=M->Dimension;
    long NodeNumber=M->NodeNumber;
    long SizeStiff=Dimension*NodeNumber+L->AdditionalForceVectDOFs;
    //Set the vectors
    VecCreate(PETSC_COMM_SELF,&U_Petsc);
    VecSetType(U_Petsc, VECSEQ);
    VecSetSizes(U_Petsc,PETSC_DECIDE,SizeStiff);
    VecSetFromOptions(U_Petsc);
    VecDuplicate(U_Petsc,&b);
    
    VecAssemblyBegin(b);
    VecAssemblyEnd(b);
    
    //Fill the  force vector
    for (PetscInt i=0; i<SizeStiff; i++)
    {
        VecSetValues(b,1,&i,&L->F_Vec[i],INSERT_VALUES);
    }
    
    //Compute the permutation
    IS rowperm=NULL,colperm=NULL;
    Mat StiffMat_Petsc_perm;
    char ordering[256] = MATORDERINGRCM;
    if (M->Mat_Ordering==1)
    {
        MatGetOrdering(StiffMat,ordering,&rowperm,&colperm);
        MatPermute(StiffMat,rowperm,colperm,&StiffMat_Petsc_perm);
        VecPermute(b,colperm,PETSC_FALSE);
         MatDestroy(&StiffMat);
        StiffMat = StiffMat_Petsc_perm;
    }
    
    if (M->SolverType=="DIRECT_MUMPS")
        SolveMUMPS_LU(M,L);
    else if (M->SolverType=="KSP")
        SolveKSP(M,L);
    
    VecAssemblyBegin(U_Petsc);
    VecAssemblyEnd(U_Petsc);
    
    //Permute the solution vector
    if (M->Mat_Ordering==1)
    {
        VecPermute(U_Petsc,colperm,PETSC_TRUE);
    }
    
    VecDestroy(&b);
    MatDestroy(&StiffMat);
    ISDestroy(&rowperm);
    ISDestroy(&colperm);
       
    for (PetscInt i=0; i<Dimension*NodeNumber; i++)
    {
        PetscScalar TempU;
        VecGetValues(U_Petsc,1,&i,&TempU);
        L->U(i)=TempU;
    }
}

void Seq_Solver::SolveMUMPS_LU(Model* M, LinearProblem* L)
{   
    MPI_Comm comm = MPI_COMM_SELF;
    KSP solver;
    PC pc;
    M->Print("Create Solver.",true);
    KSPCreate(comm,&solver);
    KSPSetOperators(solver,StiffMat,StiffMat);
    
    KSPGetPC(solver,&pc);
    M->Print("KSP Preconditioning.",true);
    KSPSetType(solver,KSPPREONLY);
    M->Print("Set LU.",true);
    PCSetType(pc,PCLU);
    M->Print("Solver MUMPS.",true);
    PCFactorSetMatSolverType(pc,MATSOLVERMUMPS);
    PCFactorSetUpMatSolverType(pc);
    
    Mat F;
    PCFactorGetMatrix(pc,&F);
    //sequential ordering
    MatMumpsSetIcntl(F,7,3);
    MatMumpsSetIcntl(F,8,77);   
    
    M->Print("Solving the system of equations.",true);
    
    //Print the PC
    if (M->PrintMat==1)
    {
        string Path=M->InputFilePath;
        Path.replace(Path.end()-3,Path.end(),"txt");
        char* PathChar=new char[Path.length()+1];
        strcpy(PathChar,Path.c_str());
        PetscViewer  viewer;
        PetscViewerCreate(PETSC_COMM_WORLD, &viewer);
        PetscViewerSetType(viewer, PETSCVIEWERASCII);
        PetscViewerFileSetMode(viewer, FILE_MODE_APPEND);
        PetscViewerFileSetName(viewer, PathChar);
        PCView(pc,viewer);
        PetscViewerASCIIOpen(PETSC_COMM_WORLD, NULL, &viewer);
        PetscViewerPushFormat(viewer, PETSC_VIEWER_ASCII_INFO);
        PetscViewerDestroy(&viewer);
    }
    
    //Solve the system
    KSPSolve(solver,b,U_Petsc);
    
    //Get the solver info
    KSPGetIterationNumber(solver,&L->SolverIterationNum);
    KSPConvergedReason IterationStopReason;
    KSPGetConvergedReason(solver,&IterationStopReason);
    L->SetConvergenceReason(IterationStopReason);
    
       //Get the condition number
    if (M->PrintConditionNumber==1)
    {
        PetscReal emax, emin;
        KSPSetComputeSingularValues(solver, PETSC_TRUE);
        KSPComputeExtremeSingularValues(solver, &emax, &emin);
        L->ConditionNumber=emax / emin;
    }
    
    KSPDestroy(&solver);
}

void Seq_Solver::SolveKSP(Model* M, LinearProblem* L)
{      
    MPI_Comm comm = MPI_COMM_SELF;
    KSP solver;
    PC pc;
    M->Print("Create KSP Solver and load preconditionner.",true);
    KSPCreate(comm,&solver);
    KSPSetOperators(solver,StiffMat,StiffMat);
    
    if (M->SolverKSP=="GMRES") 
    {
        KSPSetType(solver,KSPGMRES);
    }
    else if (M->SolverKSP=="BCGS") 
    {
        KSPSetType(solver,KSPBCGS);
    }
    else if (M->SolverKSP=="BCGSL") 
    {
        KSPSetType(solver,KSPBCGSL);
    }
    
    //Set preconditioner
    KSPGetPC(solver,&pc);
    if (M->SolverPC=="JACOBI") 
    {
        PCSetType(pc,PCJACOBI);
    }
    else if (M->SolverPC=="ILU") 
    {
        PCSetType(pc,PCILU);
        PCFactorSetLevels(pc,M->ILU_Levels);
        PCFactorSetMatOrderingType(pc,M->ILU_Ordering);
    }
    else if (M->SolverPC=="GAMG")
    {
        M->Print("Setting PC.",true);
        PCSetType(pc,PCGAMG);
        PCGAMGSetType(pc,PCGAMGAGG);
        PetscReal TempThreshold=M->AMG_Threshold;
        PetscInt ThresholdInt=1;
        PCGAMGSetThreshold(pc,&TempThreshold,ThresholdInt);
        if (M->AMG_Levels>0) PCGAMGSetNlevels(pc,M->AMG_Levels);
        if (M->AMG_SmoothN>0) PCGAMGSetNSmooths(pc,M->AMG_SmoothN);
    }
    else if (M->SolverPC=="BOOMERAMG")
    {
        M->Print("Setting PC BoomerAMG.",true);
        PCSetType(pc,PCHYPRE);
        PCHYPRESetType(pc,"boomeramg");
        PetscOptionsSetValue(NULL,"-pc_hypre_boomeramg_strong_threshold","0.05");
    }
    M->Print("Setting Ordering.",true);
    PCFactorSetMatOrderingType(pc,MATORDERINGRCM);
    PCSetUp(pc);
    PCSetFromOptions(pc);
    M->Print("PC Set-Up Complete.",true);
    
    KSPSetTolerances(solver,M->SolverTol,PETSC_DEFAULT,PETSC_DEFAULT,M->SolverMaxIt);
    KSPSetFromOptions(solver);
    KSPSetUp(solver);
    M->Print("Solving the system of equations.",true);
    
    //Print the PC
    if (M->PrintMat==1)
    {
        string Path=M->InputFilePath;
        Path.replace(Path.end()-3,Path.end(),"txt");
        char* PathChar=new char[Path.length()+1];
        strcpy(PathChar,Path.c_str());
        PetscViewer  viewer;
        PetscViewerCreate(PETSC_COMM_WORLD, &viewer);
        PetscViewerSetType(viewer, PETSCVIEWERASCII);
        PetscViewerFileSetMode(viewer, FILE_MODE_APPEND);
        PetscViewerFileSetName(viewer, PathChar);
        PCView(pc,viewer);
        PetscViewerASCIIOpen(PETSC_COMM_WORLD, NULL, &viewer);
        PetscViewerPushFormat(viewer, PETSC_VIEWER_ASCII_INFO);
        PetscViewerDestroy(&viewer);
    }
    
    //Solve the system
    KSPSolve(solver,b,U_Petsc);
    
//    //Create a viewer and print the solver info in it
//    char kspinfo[120];
//    PetscViewer viewer;
//    //Open a string viewer; then write info to it.
//    PetscViewerStringOpen(PETSC_COMM_SELF,kspinfo,120,&viewer);
//    KSPView(solver,viewer);
//    L->SolverConvergenceStatus=kspinfo;
    KSPGetIterationNumber(solver,&L->SolverIterationNum);
    KSPConvergedReason IterationStopReason;
    KSPGetConvergedReason(solver,&IterationStopReason);
    L->SetConvergenceReason(IterationStopReason);
    
    //Get the condition number
    if (M->PrintConditionNumber==1)
    {
        PetscReal emax, emin;
        KSPComputeExtremeSingularValues(solver, &emax, &emin);
        L->ConditionNumber=emax / emin;
    }
    
    KSPDestroy(&solver);    
    L->S=L->StressMat_Eigen*L->U;
}

void Seq_Solver::FillStiffMat_Petsc(Model* M, LinearProblem* L)
{
    PetscInt SizeStiff=M->Dimension*M->NodeNumber+L->AdditionalForceVectDOFs;
    MatCreate(PETSC_COMM_SELF, &StiffMat);
    MatSetType(StiffMat,MATSEQBAIJ);
    MatSetSizes(StiffMat, PETSC_DECIDE, PETSC_DECIDE, SizeStiff, SizeStiff);
    MatSetFromOptions(StiffMat);
    MatSetUp(StiffMat);
        
    //Fill the triplet vector
    PetscInt size;
    size=L->TripStiffLength+L->AdditionalDOFs+L->AdditionalForceVectDOFs;
    
    PetscInt *RowVect;
    PetscInt *ColumnVect;
    PetscScalar *ValVect;
    PetscMalloc1(size,&RowVect);
    PetscMalloc1(size,&ColumnVect);
    PetscMalloc1(size,&ValVect);
        
    long i=0;    
    while (i<L->TripStiffLength+L->AdditionalDOFs+L->AdditionalForceVectDOFs)
    {
        RowVect[i]=L->TripStiff[i].row();
        ColumnVect[i]=L->TripStiff[i].col();
        ValVect[i]=L->TripStiff[i].value();
        i++;
    }
    
    //Fill the system matrix
    M->Print("Starting filling Stiffness matrix.",true);
    MatCreateSeqAIJFromTriple(PETSC_COMM_SELF,SizeStiff,SizeStiff,RowVect,ColumnVect,ValVect,&StiffMat,L->TripStiffLength+L->AdditionalDOFs+L->AdditionalForceVectDOFs,PETSC_FALSE);
    M->Print("Stiffness matrix filled.",true);
    MatAssemblyBegin(StiffMat,MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(StiffMat,MAT_FINAL_ASSEMBLY);
    M->Print("Stiffness matrix assembled.",true);
    
    delete [] L->TripStiff;
    
    PetscScalar v;
    for (PetscInt i=0; i<SizeStiff; i++)
    {
        MatGetValues(StiffMat,1,&i,1,&i,&v);
        if (v==0)
            cout << "<!> Error row " << i << ", there is a zero on the diagonal." << endl;
    }
    
    PetscFree(RowVect);
    PetscFree(ColumnVect);
    PetscFree(ValVect);
    
 }
